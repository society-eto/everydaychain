package com.factory.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Customers;
import com.factory.common.bean.Speech;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SpeechMapper extends BaseMapper<Speech> {

    /**
     * 批量插入数据
     * @param speeches
     * @return
     */
    Integer inserts(@Param("speeches") List<Speech> speeches);

    /**
     * 数据统计接口
     * @param params
     * @return
     */
    List<Map<String,Object>> statistics(Map<String,Object> params);

    /**
     * 查询接口
     * @param params
     * @return
     */
    List<Map<String,Object>> selectList(Map<String,Object> params);

}