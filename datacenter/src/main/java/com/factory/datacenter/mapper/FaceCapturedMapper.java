package com.factory.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Customers;
import com.factory.common.bean.FaceCaptured;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FaceCapturedMapper extends BaseMapper<FaceCaptured> {

    /**
     * 查询列表
     * @param params
     * @return
     */
    List<Map<String,Object>> queryList(Map<String,Object> params);

    /**
     * 查询列表
     * @param params
     * @return
     */
    Integer queryTotal(Map<String,Object> params);
}