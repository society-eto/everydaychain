package com.factory.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Camera;
import com.factory.common.bean.CustomersDoorway;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CameraMapper extends BaseMapper<Camera> {


    /**
     * 批量插入数据
     * @param cameras
     * @return
     */
    Integer inserts(@Param("cameras") List<Camera> cameras);

    /**
     * 摄像头数据统计
     *
     * @param params
     * @return
     */
    Map<String, Object> statistics(Map<String, Object> params);


    /**
     * 更新摄像头的状态
     * @Param cameraIds
     * @Param deviceId
     * @return
     */
    Integer updateStatus(@Param("cameraIds") List<String> cameraIds, @Param("deviceId") String deviceId);
}