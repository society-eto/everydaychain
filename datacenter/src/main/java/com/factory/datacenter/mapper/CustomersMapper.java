package com.factory.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Customers;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CustomersMapper extends BaseMapper<Customers> {

    /**
     * 批量插入数据
     * @param customers
     * @return
     */
    Integer inserts(@Param("customers") List<Customers> customers);

    /**
     * 按季度统计
     * @param param
     * @return
     */
    List<Map<String,Object>> statisticsQuarter(Map<String,Object> param);

    /**
     * 按月统计
     * @param param
     * @return
     */
    List<Map<String,Object>> statisticsMonth(Map<String,Object> param);

    /**
     *按周统计
     * @param param
     * @return
     */
    List<Map<String,Object>> statisticsWeek(Map<String,Object> param);


    /**
     *按日统计
     * @param param
     * @return
     */
    List<Map<String,Object>> statisticsDay(Map<String,Object> param);

    /**
     *按性别统计
     * @param param
     * @return
     */
    List<Map<String,Object>> statisticsGender(Map<String,Object> param);

    /**
     *按年龄统计
     * @param param
     * @return
     */
    List<Map<String,Object>> statisticsAge(Map<String,Object> param);

    /**
     *按年龄统计
     * @param param
     * @return
     */
    List<Map<String,Object>> statisticsAgeCustomers(Map<String,Object> param);

    /**
     *按店铺统计
     * @param param
     * @return
     */
    List<Map<String,Object>> statisticsLocation(Map<String,Object> param);

    /**
     *按店铺统计
     * @param param
     * @return
     */
    List<Map<String,Object>> statisticsCustomers(Map<String,Object> param);
}