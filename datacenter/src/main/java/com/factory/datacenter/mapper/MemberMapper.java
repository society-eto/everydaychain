package com.factory.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Member;

public interface MemberMapper extends BaseMapper<Member> {

}