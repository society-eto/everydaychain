package com.factory.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.CameraWheelLog;

public interface CameraWheelLogMapper extends BaseMapper<CameraWheelLog> {
}