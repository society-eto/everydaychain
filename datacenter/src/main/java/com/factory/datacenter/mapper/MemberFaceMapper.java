package com.factory.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.MemberFace;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MemberFaceMapper extends BaseMapper<MemberFace> {

    /**
     * 批量写入特征值数据
     *
     * @param memberFaces
     * @return
     */
    Integer inserts(@Param("memberFaces") List<MemberFace> memberFaces);
}