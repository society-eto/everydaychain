package com.factory.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.CustomersDoorway;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CustomersDoorwayMapper extends BaseMapper<CustomersDoorway> {

    /**
     * 批量插入数据
     *
     * @param customersDoorways
     * @return
     */
    Integer inserts(@Param("customersDoorways") List<CustomersDoorway> customersDoorways);

    /**
     * 数据统计接口
     *
     * @param params
     * @return
     */
    List<Map<String, Object>> statistics(Map<String, Object> params);
}