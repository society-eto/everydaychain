package com.factory.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Turnover;

import java.util.List;
import java.util.Map;

public interface TurnoverMapper extends BaseMapper<Turnover> {

    /**
     * 营业数据统计接口
     * @param params
     * @return
     */
    List<Map<String,Object>> statistics(Map<String,Object> params);

}