package com.factory.datacenter.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Alarm;

import java.util.List;
import java.util.Map;

public interface AlarmMapper extends BaseMapper<Alarm> {

    /**
     * 告警数据统计
     * @param params
     * @return
     */
    Map<String,Object> statistics(Map<String,Object> params);

    /**
     * 告警信息统计
     * @param params
     * @return
     */
    List<Map<String, Object>> statisticsAlarm(Map<String, Object> params);

    /**
     * 日统计告警信息统计
     * @param params
     * @return
     */
    List<Map<String, Object>> statisticsDay(Map<String, Object> params);

    /**
     * 周统计告警信息统计
     * @param params
     * @return
     */
    List<Map<String, Object>> statisticsWeek(Map<String, Object> params);

    /**
     * 月统计告警信息统计
     * @param params
     * @return
     */
    List<Map<String, Object>> statisticsMonth(Map<String, Object> params);

    /**
     * 年统计告警信息统计
     * @param params
     * @return
     */
    List<Map<String, Object>> statisticsQuarter(Map<String, Object> params);
}