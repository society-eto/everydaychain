package com.factory.datacenter.controller;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Camera;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;
import com.factory.datacenter.mapper.CameraMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/datacenter/camera")
public class CameraController extends BaseController<Camera>{

    @Autowired
    private CameraMapper cameraMapper;

    @Override
    protected BaseMapper<Camera> getBaseMapper() {
        return cameraMapper;
    }


    /**
     * 批量写入数据
     *
     * @param cameras
     * @return
     */
    @PostMapping("/inserts")
    public Result inserts(@RequestBody List<Camera> cameras) {
        log.info("批量写入客流数据");
        Integer inserts = cameraMapper.inserts(cameras);
        return Result.success(inserts);
    }

    /**
     * 摄像头数据统计
     * @param params
     * @return
     */
    @PostMapping("/statistics")
    public Result statistics(@RequestBody Map<String,Object> params){
        return Result.success(cameraMapper.statistics(params));
    }

    /**
     * 更新摄像头的状态
     * @Param cameraIds
     * @Param deviceId
     * @return
     */
    @PostMapping("/updateStatus")
    public Result updateStatus(@RequestBody Map<String,Object> params){
        List<String> cameraIds;

        if(null == params.get("cameraIds")){
            cameraIds = new ArrayList<>();
        }else {
            cameraIds = (List<String>) params.get("cameraIds");
        }

        cameraIds.add("9999");

        return Result.success(cameraMapper.updateStatus(cameraIds,StringUtil.val(params.get("deviceId"), "")));
    }
}
