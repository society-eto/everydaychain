package com.factory.datacenter.controller;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.CustomersDoorway;
import com.factory.common.pojo.Result;
import com.factory.datacenter.mapper.CustomersDoorwayMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 门口客流统计数据
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 15:19
 */
@RestController
@RequestMapping("/datacenter/customersDoorway")
public class CustomersDoorwayController extends BaseController<CustomersDoorway> {

    @Autowired
    private CustomersDoorwayMapper customersDoorwayMapper;


    @Override
    protected BaseMapper<CustomersDoorway> getBaseMapper() {
        return customersDoorwayMapper;
    }

    /**
     * 批量写入数据
     *
     * @param customersDoorways
     * @return
     */
    @PostMapping("/inserts")
    public Result inserts(@RequestBody List<CustomersDoorway> customersDoorways) {
        log.info("批量写入客流数据");
        Integer inserts = customersDoorwayMapper.inserts(customersDoorways);
        return Result.success(inserts);
    }

    /**
     * 营业数据统计接口
     *
     * @param params
     * @return
     */
    @PostMapping("/statistics")
    public Result statistics(@RequestBody Map<String, Object> params) {
        return Result.success(customersDoorwayMapper.statistics(params));
    }
}
