package com.factory.datacenter.controller;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.MemberFace;
import com.factory.datacenter.mapper.MemberFaceMapper;
import com.factory.common.pojo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/datacenter/memberFace")
public class MemberFaceController extends BaseController<MemberFace>{

    @Autowired
    private MemberFaceMapper memberFaceMapper;

    @Override
    protected BaseMapper<MemberFace> getBaseMapper() {
        return memberFaceMapper;
    }

    /**
     * 批量写入特征值数据
     *
     * @param memberFaces
     * @return
     */
    @PostMapping("/inserts")
    public Result inserts(@RequestBody List<MemberFace> memberFaces) {
        log.info("批量写入特征值数据");
        Integer inserts = memberFaceMapper.inserts(memberFaces);
        return Result.success(inserts);
    }
}
