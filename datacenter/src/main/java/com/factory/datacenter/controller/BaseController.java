package com.factory.datacenter.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.factory.common.bean.BaseBean;
import com.factory.common.core.BaseService;
import com.factory.common.core.SqlCondition;
import com.factory.common.core.ValidateType;
import com.factory.common.pojo.QueryListParam;
import com.factory.common.pojo.Result;
import com.factory.common.pojo.UpdateWrapperBody;
import com.factory.common.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 持久层微服务用于连接数据中心，提供数据服务
 *
 * @param <T>
 */
public abstract class BaseController<T extends BaseBean> {

    protected static final Logger log = LoggerFactory.getLogger(BaseController.class);

    protected abstract BaseMapper<T> getBaseMapper();

    /**
     * T
     * 通用的添加方法
     *
     * @return 用户列表
     */

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Result insert(@RequestBody T t) {
        log.info("通用的添加数据方法,{}", t);
        if (t == null) {
            return Result.fail(500, "request body can not null!");
        }
        //校验
        Optional<Result> validate = t.validate(ValidateType.INSERT);
        if (validate.isPresent()) {
            return validate.get();
        }

        Integer result = getBaseMapper().insert(t);
        if (1 == result) {
            return Result.success(null, "添加成功!");
        }

        return Result.fail(504, "insert fail!");
    }

    /**
     * 通用的查一条数据方法
     *
     * @return 一条数据
     */
    @RequestMapping(value = "/query/{id}/{deviceId}", method = RequestMethod.GET)
    public Result query(@PathVariable(name = "id") String id, @PathVariable(name = "deviceId")String deviceId) {
        log.info("通用的查一条数据方法,{}", id);
        if (id == null) {
            return Result.fail(500, "request id and device id can not null!");
        }

        QueryWrapper<T> wrapper = new QueryWrapper<T>().eq("id", id);
        if(StringUtil.notEmpty(deviceId) && !"none".equals(deviceId)){
            wrapper.eq("device_id", deviceId);
        }

        return Result.success(getBaseMapper().selectOne(wrapper), "操作成功");
    }

    /**
     * 通用的删除一条数据方法
     *
     * @return 删除一条数据
     */
    @RequestMapping(value = "/delete/{id}/{deviceId}", method = RequestMethod.GET)
    public Result delete(@PathVariable(name = "id") String id, @PathVariable(name = "deviceId") String deviceId) {
        log.info("通用的查一条数据方法,{}", id);
        if (StringUtil.isEmpty(id) || StringUtil.isEmpty(deviceId)) {
            return Result.fail(500, "request id and device id can not null!");
        }

        Map<String, Object> param = new HashMap<>();
        param.put("id", id);
        if(StringUtil.notEmpty(deviceId) && !"none".equals(deviceId)){
            param.put("device_id", deviceId);
        }

        Integer result = getBaseMapper().deleteByMap(param);

        if (0 < result) {
            return Result.success(null, "删除成功!");
        }

        return Result.fail(500, "删除失败!");
    }

    /**
     * 通用的修改一条数据方法
     *
     * @return 修改一条数据
     */

    @RequestMapping(value = "/update/{id}/{deviceId}", method = RequestMethod.POST)
    public Result update(@RequestBody T t, @PathVariable(name = "id") String id, @PathVariable(name = "deviceId") String deviceId) {
        log.info("通用的修改一条数据方法,{}", t);
        if (t == null) {
            return Result.fail(500, "request update body can not null!");
        }
        if (StringUtil.isEmpty(id) || StringUtil.isEmpty(deviceId)) {
            return Result.fail(500, "request id and device id can not null!");
        }

        //校验
        Optional<Result> validate = t.validate(ValidateType.UPDATE);
        if (validate.isPresent()) {
            return validate.get();
        }

        QueryWrapper<T> wrapper = new QueryWrapper<T>().eq("id", id);
        if(StringUtil.notEmpty(deviceId) && !"none".equals(deviceId)){
            wrapper.eq("device_id", deviceId);
        }

        Integer result = getBaseMapper().update(t, wrapper);
        if (0 < result) {
            return Result.success(null, "修改成功!");
        }
        return Result.fail(500, "失败!");
    }

    /**
     * 通用的修改一条数据方法
     *
     * @return 修改一条数据
     */

    @RequestMapping(value = "/updateWrapper", method = RequestMethod.POST)
    public Result updateWrapper(@RequestBody UpdateWrapperBody<T> updateWrapperBody) {
        log.info("通用的修改一条数据方法");
        if (updateWrapperBody == null) {
            return Result.fail(500, "request update body can not null!");
        }
        if (StringUtil.isEmpty(updateWrapperBody.getConditions())) {
            return Result.fail(500, "request updateWrapper can not null!");
        }

        UpdateWrapper<T> wrapper = new UpdateWrapper<>();
        for(SqlCondition e : updateWrapperBody.getConditions()){
            if(StringUtil.notEmpty(e.getValue())){
                switch (e.getMethod()) {
                    case SqlCondition.Conditions.EQ:
                        wrapper.eq(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.LIKE:
                        wrapper.like(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.GE:
                        wrapper.ge(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.LE:
                        wrapper.le(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.NE:
                        wrapper.ne(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                }
            }
        }

        return Result.success(getBaseMapper().update(updateWrapperBody.getT(), wrapper), "修改成功!");
    }

    /**
     * 通用查询数据列表的方法，含有分页的功能
     * 子类继承父类即可使用该方法
     * {
     * "pageSize":10,
     * "pageNum":1,
     * "conditions":[{"name":"account","value":"123456","method":"eq"}]
     * }
     *
     * @return 查询到的数据列表集合
     */

    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    public Result queryList(@RequestBody QueryListParam param) {
        log.info("通用查询数据列表的方法,{}", param);

        if (param == null) {
            return Result.fail(500, "request param body can not null!");
        }

        Page<T> pageEntity = new Page<>();
        pageEntity.setSize(StringUtil.toInteger(param.getPageSize(), 10));
        pageEntity.setCurrent(StringUtil.toInteger(param.getPageNum(), 1));

        QueryWrapper<T> wrapper = new QueryWrapper<>();
        SqlCondition order = null;
        for(SqlCondition e : param.getConditions()){
            if(StringUtil.notEmpty(e.getValue())){
                switch (e.getMethod()) {
                    case SqlCondition.Conditions.EQ:
                        wrapper.eq(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.LIKE:
                        wrapper.like(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.GE:
                        wrapper.ge(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.LE:
                        wrapper.le(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.NE:
                        wrapper.ne(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.DESC:
                        order = e;
                        break;
                    case SqlCondition.Conditions.ASC:
                        order = e;
                        break;
                }
            }
        }

        if(null != order){
            if(SqlCondition.Conditions.DESC.equals(order.getMethod())){
                wrapper.orderByDesc(StringUtil.underscoreName(order.getKey()));
            }else {
                wrapper.orderByAsc(StringUtil.underscoreName(order.getKey()));
            }
        }

        return Result.success(getBaseMapper().selectPage(pageEntity, wrapper), "操作成功");
    }
}