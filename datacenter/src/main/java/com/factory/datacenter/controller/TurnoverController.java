package com.factory.datacenter.controller;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Turnover;
import com.factory.common.pojo.Result;
import com.factory.datacenter.mapper.TurnoverMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 营业数据统计控制器
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 15:24
 */
@RequestMapping("/datacenter/turnover")
@RestController
public class TurnoverController extends BaseController<Turnover> {

    @Autowired
    private TurnoverMapper turnoverMapper;

    @Override
    protected BaseMapper<Turnover> getBaseMapper() {
        return turnoverMapper;
    }

    /**
     * 营业数据统计接口
     *
     * @param params
     * @return
     */
    @PostMapping("/statistics")
    public Result statistics(@RequestBody Map<String, Object> params) {
        return Result.success(turnoverMapper.statistics(params));
    }
}
