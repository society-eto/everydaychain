package com.factory.datacenter.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Customers;
import com.factory.common.bean.MemberFace;
import com.factory.common.pojo.Result;
import com.factory.common.utils.DateUtil;
import com.factory.common.utils.StringUtil;
import com.factory.datacenter.mapper.CustomersMapper;
import com.factory.datacenter.mapper.MemberFaceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 客流数据控制器
 */
@RestController
@RequestMapping("/datacenter/customers")
public class CustomersController extends BaseController<Customers> {

    @Autowired
    private CustomersMapper customersMapper;

    @Autowired
    private MemberFaceMapper memberFaceMapper;

    @Override
    protected BaseMapper getBaseMapper() {
        return customersMapper;
    }

    /**
     * 批量写入客流数据
     *
     * @param customers
     * @return
     */
    @PostMapping("/inserts")
    public Result inserts(@RequestBody List<Customers> customers) {
        log.info("批量写入客流数据");
        Integer inserts = customersMapper.inserts(customers);
        return Result.success(inserts);
    }

    /**
     * 客流数据日统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsDay")
    public Result statisticsDay(@RequestBody Map<String, Object> params) {
        log.info("客流数据日统计,{}", params);
        if (StringUtil.isEmpty(params.get("endDate"))) {
            params.put("endDate", DateUtil.getDateString(new Date(), "yyyy-MM-dd HH:mm:ss"));
        }
        return Result.success(customersMapper.statisticsDay(params));
    }

    /**
     * 客流数据周统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsWeek")
    public Result statisticsWeek(@RequestBody Map<String, Object> params) {
        log.info("客流数据周统计,{}", params);
        if (StringUtil.isEmpty(params.get("endDate"))) {
            params.put("endDate", DateUtil.getDateString(new Date(), "yyyy-MM-dd HH:mm:ss"));
        }
        return Result.success(customersMapper.statisticsWeek(params));
    }

    /**
     * 客流数据月统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsMonth")
    public Result statisticsMonth(@RequestBody Map<String, Object> params) {
        log.info("客流数据月统计,{}", params);
        if (StringUtil.isEmpty(params.get("endDate"))) {
            params.put("endDate", DateUtil.getDateString(new Date(), "yyyy-MM-dd HH:mm:ss"));
        }
        return Result.success(customersMapper.statisticsMonth(params));
    }

    /**
     * 客流数据季度统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsQuarter")
    public Result statisticsQuarter(@RequestBody Map<String, Object> params) {
        log.info("客流数据季度统计,{}", params);
        if (StringUtil.isEmpty(params.get("endDate"))) {
            params.put("endDate", DateUtil.getDateString(new Date(), "yyyy-MM-dd HH:mm:ss"));
        }
        return Result.success(customersMapper.statisticsQuarter(params));
    }

    /**
     * 性别数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsGender")
    public Result statisticsGender(@RequestBody Map<String, Object> params) {
        log.info("客流数据月统计,{}", params);
        if (StringUtil.isEmpty(params.get("endDate"))) {
            params.put("endDate", DateUtil.getDateString(new Date(), "yyyy-MM-dd HH:mm:ss"));
        }
        return Result.success(customersMapper.statisticsGender(params));
    }

    /**
     * 年龄数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsAge")
    public Result statisticsAge(@RequestBody Map<String, Object> params) {
        log.info("客流数据月统计,{}", params);
        if (StringUtil.isEmpty(params.get("endDate"))) {
            params.put("endDate", DateUtil.getDateString(new Date(), "yyyy-MM-dd HH:mm:ss"));
        }
        return Result.success(customersMapper.statisticsAge(params));
    }

    /**
     * 年龄数据统计 statisticsAgeCustomers
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsAgeCustomers")
    public Result statisticsAgeCustomers(@RequestBody Map<String, Object> params) {
        log.info("客流数据月统计,{}", params);
        if (StringUtil.isEmpty(params.get("endDate"))) {
            params.put("endDate", DateUtil.getDateString(new Date(), "yyyy-MM-dd HH:mm:ss"));
        }
        return Result.success(customersMapper.statisticsAgeCustomers(params));
    }

    /**
     * 店铺数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsLocation")
    public Result statisticsLocation(@RequestBody Map<String, Object> params) {
        log.info("客流数据月统计,{}", params);
        if (StringUtil.isEmpty(params.get("endDate"))) {
            params.put("endDate", DateUtil.getDateString(new Date(), "yyyy-MM-dd HH:mm:ss"));
        }
        return Result.success(customersMapper.statisticsLocation(params));
    }

    /**
     * 店铺数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsCustomers")
    public Result statisticsCustomers(@RequestBody Map<String, Object> params) {
        log.info("客流数据月统计,{}", params);
        if (StringUtil.isEmpty(params.get("endDate"))) {
            params.put("endDate", DateUtil.getDateString(new Date(), "yyyy-MM-dd HH:mm:ss"));
        }
        return Result.success(customersMapper.statisticsCustomers(params));
    }

    /**
     * @param params
     * @return
     */
    @PostMapping("/create")
    public Result create(@RequestBody Map<String, Object> params) {
        log.info("客流数据月统计,{}", params);
        Long date = Long.valueOf(params.get("date").toString());
        List<MemberFace> memberFaceList = memberFaceMapper.selectList(new QueryWrapper<MemberFace>().eq("user_id", params.get("userId")));

        List<Customers> customersItem = new ArrayList<>();
        for (MemberFace memberFace : memberFaceList) {

            Customers e = new Customers();
            e.setDeviceId(memberFace.getDeviceId());
            e.setDeviceName(memberFace.getDeviceName());
            e.setUserId(memberFace.getUserId());
            e.setUserName(memberFace.getUserName());
            e.setLocationId(memberFace.getLocationId());
            e.setLocationName(memberFace.getLocationName());
            e.setId(StringUtil.getUUID());
            e.setMemberFaceId(memberFace.getId());
            e.setAge(memberFace.getAge());
            e.setGender(memberFace.getGander());
            int i = new Random().nextInt(1440);
            log.debug("i,{}", i);
            e.setCreateDate(new Date(date + i * 60000));

            e.setCustomersType(StringUtil.isEmpty(memberFace.getMemberId()) ? (new Random().nextInt(100) % 2) : 2);

            customersItem.add(e);
        }

        customersMapper.inserts(customersItem);

        return Result.success();
    }
}
