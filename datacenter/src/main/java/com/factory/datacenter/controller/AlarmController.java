package com.factory.datacenter.controller;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Alarm;
import com.factory.datacenter.mapper.AlarmMapper;
import com.factory.common.pojo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/datacenter/alarm")
public class AlarmController extends BaseController<Alarm> {

    @Autowired
    private AlarmMapper alarmMapper;

    @Override
    protected BaseMapper<Alarm> getBaseMapper() {
        return alarmMapper;
    }

    /**
     * 告警数据统计
     * @param params
     * @return
     */
    @PostMapping("/statistics")
    public Result statistics(@RequestBody Map<String,Object> params){
        log.info("告警数据统计,{}",params);
        return Result.success(alarmMapper.statistics(params));
    }

    /**
     * 告警信息统计
     * @param params
     * @return
     */
    @PostMapping("/statisticsAlarm")
    public Result statisticsAlarm(@RequestBody Map<String, Object> params){
        log.info("告警信息统计,{}",params);
        return Result.success(alarmMapper.statisticsAlarm(params));
    }

    /**
     * 日统计告警信息统计
     * @param params
     * @return
     */
    @PostMapping("/statisticsDay")
    public Result statisticsDay(@RequestBody Map<String, Object> params){
        log.info("日统计告警信息统计,{}",params);
        return Result.success(alarmMapper.statisticsDay(params));
    }

    /**
     * 周统计告警信息统计
     * @param params
     * @return
     */
    @PostMapping("/statisticsWeek")
    public Result statisticsWeek(@RequestBody Map<String, Object> params){
        log.info("周统计告警信息统计,{}",params);
        return Result.success(alarmMapper.statisticsWeek(params));
    }

    /**
     * 周统计告警信息统计
     * @param params
     * @return
     */
    @PostMapping("/statisticsMonth")
    public Result statisticsMonth(@RequestBody Map<String, Object> params){
        log.info("周统计告警信息统计,{}",params);
        return Result.success(alarmMapper.statisticsMonth(params));
    }

    /**
     * 年统计告警信息统计
     * @param params
     * @return
     */
    @PostMapping("/statisticsQuarter")
    public Result statisticsQuarter(@RequestBody Map<String, Object> params){
        log.info("年统计告警信息统计,{}",params);
        return Result.success(alarmMapper.statisticsQuarter(params));
    }
}
