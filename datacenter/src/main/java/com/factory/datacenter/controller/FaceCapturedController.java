package com.factory.datacenter.controller;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.FaceCaptured;
import com.factory.common.core.SqlCondition;
import com.factory.common.pojo.QueryListParam;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;
import com.factory.datacenter.mapper.FaceCapturedMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/datacenter/faceCaptured")
public class FaceCapturedController extends BaseController<FaceCaptured> {

    @Autowired
    private FaceCapturedMapper faceCapturedMapper;

    @Override
    protected BaseMapper<FaceCaptured> getBaseMapper() {
        return faceCapturedMapper;
    }

    @Override
    public Result queryList(@RequestBody QueryListParam param) {
        Map<String, Object> params = new HashMap<>();

        for (SqlCondition e : param.getConditions()) {
            if (StringUtil.notEmpty(e.getValue())) {

                if ("createDate".equals(e.getKey()) && SqlCondition.Conditions.GE.equals(e.getMethod()))
                    params.put("beginDate", e.getValue());
                else if ("createDate".equals(e.getKey()) && SqlCondition.Conditions.LE.equals(e.getMethod()))
                    params.put("endDate", e.getValue());
                else if ("age".equals(e.getKey()) && SqlCondition.Conditions.GE.equals(e.getMethod()))
                    params.put("beginAge", e.getValue());
                else if ("age".equals(e.getKey()) && SqlCondition.Conditions.LE.equals(e.getMethod()))
                    params.put("endAge", e.getValue());
                else
                    params.put(e.getKey(), e.getValue());

            }
        }

        params.put("page", (param.getPageNum() - 1) * param.getPageSize());
        params.put("pageSize", param.getPageSize());

        List<Map<String, Object>> maps = faceCapturedMapper.queryList(params);
        Integer integer = faceCapturedMapper.queryTotal(params);

        Map<String, Object> result = new HashMap<>();
        result.put("total", integer);
        result.put("records", maps);
        result.put("size", param.getPageSize());
        result.put("current", param.getPageNum());

        return Result.success(result);
    }
}
