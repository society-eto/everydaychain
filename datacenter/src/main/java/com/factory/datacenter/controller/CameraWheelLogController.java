package com.factory.datacenter.controller;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.CameraWheelLog;
import com.factory.datacenter.mapper.CameraWheelLogMapper;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡检日控制器
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 15:17
 */
@RestController
@RequestMapping("/datacenter/cameraWheelLog")
public class CameraWheelLogController extends BaseController<CameraWheelLog> {

    @Autowired
    private CameraWheelLogMapper cameraWheelLogMapper;

    @Override
    protected BaseMapper<CameraWheelLog> getBaseMapper() {
        return cameraWheelLogMapper;
    }
}
