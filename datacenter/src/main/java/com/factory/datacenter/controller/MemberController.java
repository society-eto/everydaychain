package com.factory.datacenter.controller;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Member;
import com.factory.datacenter.mapper.MemberMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/datacenter/member")
public class MemberController extends BaseController<Member> {

    @Autowired
    private MemberMapper memberMapper;

    @Override
    protected BaseMapper<Member> getBaseMapper() {
        return memberMapper;
    }
}
