package com.factory.datacenter.controller;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Speech;
import com.factory.common.pojo.Result;
import com.factory.datacenter.mapper.SpeechMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 语音识别数据统计控制类
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 15:22
 */
@RestController
@RequestMapping("/datacenter/speech")
public class SpeechController extends BaseController<Speech> {

    @Autowired
    private SpeechMapper speechMapper;

    @Override
    protected BaseMapper<Speech> getBaseMapper() {
        return speechMapper;
    }


    /**
     * 批量写入数据
     *
     * @param speeches
     * @return
     */
    @PostMapping("/inserts")
    public Result inserts(@RequestBody List<Speech> speeches) {
        log.info("批量写入客流数据");
        Integer inserts = speechMapper.inserts(speeches);
        return Result.success(inserts);
    }

    /**
     * 数据统计接口
     *
     * @param params
     * @return
     */
    @PostMapping("/statistics")
    public Result statistics(@RequestBody Map<String, Object> params) {
        return Result.success(speechMapper.statistics(params));
    }

    /**
     * 查询接口
     * @param params
     * @return
     */
    @PostMapping("/selectList")
    public Result select(@RequestBody Map<String,Object> params) {
        return Result.success(speechMapper.selectList(params));
    }

}
