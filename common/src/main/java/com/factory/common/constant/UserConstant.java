package com.factory.common.constant;

public interface UserConstant {

    String USERTOKEN = "UserToken";

    String LONINUSER = "LoginUser_%s";

    static String getToken(String userToken){
        return String.format(LONINUSER,userToken);
    }

    interface Status{
        Integer DELETE = -1;
        Integer NORMAL = 1;
    }
}
