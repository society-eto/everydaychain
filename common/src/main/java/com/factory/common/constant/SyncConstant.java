package com.factory.common.constant;

/**
 * 数据同步常量类
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/28 17:07
 */
public class SyncConstant {

    public static final Integer INSERT = 1;
    public static final Integer DELETE = 2;
    public static final Integer UPDATE = 3;
}
