package com.factory.common.constant;

public enum ResultCode {

    NoLogin(-1,"请您先登录"),
    NoAuth(-3,"您没有访问权限"),
    LoginExpired(-2,"您登录已经过期，请重新登录"),
    NotBind(-4,"您未绑定账号请先绑定账号"),
    InvalidDevice(-5,"无效的设备");

    public int code;

    public String message;

    ResultCode(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
