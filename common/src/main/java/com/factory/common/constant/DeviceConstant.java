package com.factory.common.constant;

/**
 * 设备常量类
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/29 10:43
 */
public interface DeviceConstant {

    String SERIALNO = "SerialNo";

    interface Status{
        Integer DELETE = -1;
        Integer NORMAL = 1;
        Integer INVALID = 2;
        Integer EXPIRE = 3;
    }

    public enum Functions{

        helmet("helmet","安全帽","请您佩戴好安全帽"),
        cross("cross","越线","请勿越线"),
        smoking("smoking","吸烟","请勿吸烟"),
        fire("fire","明火","发现明火"),
        smog("smog","烟雾","发现烟雾"),
        uniform("uniform","工服","请您穿戴好工服"),
        face("face","人脸","检测到人脸");

        public String key;
        public String value;
        public String desc;

        Functions(String key, String value,String desc) {
            this.key = key;
            this.value = value;
            this.desc = desc;
        }
    }
}
