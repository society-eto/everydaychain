package com.factory.common.core;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.factory.common.bean.BaseBean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public abstract class BaseServiceImpl<T extends BaseBean> implements BaseService<T> {

	protected abstract BaseMapper<T> getBaseMapper();

	@Override
	public Integer insert(T var1) {
		return getBaseMapper().insert(var1);
	}

	@Override
	public Integer deleteById(Serializable var1) {
		return getBaseMapper().deleteById(var1);
	}

	@Override
	public Integer deleteByMap(Map<String, Object> var1) {
		return getBaseMapper().deleteByMap(var1);
	}

	@Override
	public Integer delete(Wrapper<T> var1) {
		return getBaseMapper().delete(var1);
	}

	@Override
	public Integer deleteBatchIds(List<? extends Serializable> var1) {
		return getBaseMapper().deleteBatchIds(var1);
	}

	@Override
	public Integer updateById(T var1) {
		return getBaseMapper().updateById(var1);
	}

	@Override
	public Integer update(T var1, Wrapper<T> var2) {
		return getBaseMapper().update(var1,var2);
	}

	@Override
	public T selectById(Serializable var1) {
		return getBaseMapper().selectById(var1);
	}

	@Override
	public List<T> selectBatchIds(List<? extends Serializable> var1) {
		return getBaseMapper().selectBatchIds(var1);
	}

	@Override
	public List<T> selectByMap(Map<String, Object> var1) {
		return getBaseMapper().selectByMap(var1);
	}

	@Override
	public T selectOne(Wrapper<T> queryWrapper) {
		return getBaseMapper().selectOne(queryWrapper);
	}

	@Override
	public Integer selectCount(Wrapper<T> var1) {
		return getBaseMapper().selectCount(var1);
	}

	@Override
	public List<T> selectList(Wrapper<T> var1) {
		return getBaseMapper().selectList(var1);
	}

	@Override
	public List<Map<String, Object>> selectMaps(Wrapper<T> var1) {
		return getBaseMapper().selectMaps(var1);
	}

	@Override
	public List<Object> selectObjs(Wrapper<T> var1) {
		return getBaseMapper().selectObjs(var1);
	}

	@Override
	public IPage<T> selectPage(Page var1, Wrapper<T> var2) {
		return getBaseMapper().selectPage(var1, var2);
	}
}