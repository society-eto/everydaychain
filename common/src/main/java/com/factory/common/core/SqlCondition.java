package com.factory.common.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SqlCondition {

    private String key;

    private String value;

    private String method;

    public interface  Conditions{
        String EQ = "eq";
        String LIKE = "like";
        String GE = "ge";
        String LE = "le";
        String NE = "ne";
        String GT = "gt";
        String LT = "lt";
        String DESC = "desc";
        String ASC = "asc";
    }
}