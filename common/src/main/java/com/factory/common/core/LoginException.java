package com.factory.common.core;

public class LoginException extends BaseException {

    public LoginException(int code, String message) {
        super(code, message);
    }
    
}
