package com.factory.common.core;

public class ServiceException extends BaseException {

    private int code;

    private String message;

    public ServiceException(int code,String message){
        super(code,message);
        this.code = code;
        this.message = message;
    }

    public ServiceException(String message){
        super(500,message);
        this.code = 500;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
