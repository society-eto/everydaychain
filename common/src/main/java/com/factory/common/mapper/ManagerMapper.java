package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Manager;

public interface ManagerMapper extends BaseMapper<Manager> {

}