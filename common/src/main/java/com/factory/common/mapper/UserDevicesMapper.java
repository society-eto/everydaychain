package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.UserDevices;

import java.util.Map;

public interface UserDevicesMapper extends BaseMapper<UserDevices> {

    /**
     * 设备数据统计
     * @param params
     * @return
     */
    Map<String,Object> statistics(Map<String,Object> params);

}