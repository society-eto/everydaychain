package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.DevicesAlgorithm;

public interface DevicesAlgorithmMapper extends BaseMapper<DevicesAlgorithm> {

}