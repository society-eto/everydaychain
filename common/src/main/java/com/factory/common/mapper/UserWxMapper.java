package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.UserWx;

public interface UserWxMapper extends BaseMapper<UserWx> {
}