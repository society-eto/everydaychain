package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Config;

public interface ConfigMapper extends BaseMapper<Config> {

}