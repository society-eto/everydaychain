package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据账号查询用户信息
     * @param account
     * @return
     */
    User queryByAccount(@Param("account") String account);

    /**
     * 根据openid查询用户信息
     * @param openId
     * @return
     */
    User queryByOpenId(String openId);
}