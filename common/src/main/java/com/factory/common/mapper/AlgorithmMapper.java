package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Algorithm;

import java.util.List;
import java.util.Map;

public interface AlgorithmMapper extends BaseMapper<Algorithm> {

    List<Algorithm> devicesAlgorithmList(Map<String, Object> params);
}