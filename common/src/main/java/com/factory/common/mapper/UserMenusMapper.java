package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.UserMenus;

public interface UserMenusMapper extends BaseMapper<UserMenus> {
}