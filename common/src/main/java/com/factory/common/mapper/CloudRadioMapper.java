package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.CloudRadio;

public interface CloudRadioMapper extends BaseMapper<CloudRadio> {

}