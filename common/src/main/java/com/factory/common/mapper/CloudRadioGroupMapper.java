package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.CloudRadioGroup;

public interface CloudRadioGroupMapper extends BaseMapper<CloudRadioGroup> {

}