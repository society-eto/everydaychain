package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Menus;
import com.factory.common.pojo.MenusPojo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenusMapper extends BaseMapper<Menus> {

    /**
     * 查询当前用户所有的菜单
     * @param userId 当前用户id
     * @param parentId 当前菜单父id
     * @return
     */
    List<MenusPojo> queryByUser(@Param("userId") String userId, @Param("parentId") String parentId);
}