package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Location;

public interface LocationMapper extends BaseMapper<Location> {
}