package com.factory.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Devices;

import java.util.Map;

public interface DevicesMapper extends BaseMapper<Devices> {
}