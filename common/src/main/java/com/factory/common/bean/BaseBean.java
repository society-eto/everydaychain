package com.factory.common.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import com.factory.common.core.ValidateType;
import com.factory.common.pojo.Result;

import java.util.Optional;

public class BaseBean {

    public Optional<Result> validate(){
        return Optional.empty();
    }

    public Optional<Result> validate(ValidateType method){
        switch (method){
            case QUERY: return vQuery();
            case DETELE: return vDelete();
            case INSERT: return vInsert();
            case UPDATE: return vUpdate();
            case QUERYLIST: return vQueryList();
        }
        return Optional.empty();
    }

    protected Optional<Result> vInsert(){
       return Optional.empty();
    }

    protected Optional<Result> vDelete(){
       return Optional.empty();
    }

    protected Optional<Result> vUpdate(){
       return Optional.empty();
    }

    protected Optional<Result> vQuery(){
       return Optional.empty();
    }

    protected Optional<Result> vQueryList(){
       return Optional.empty();
    }

    @TableField(exist = false)
    private  User user;

    public void setUser(User user){
        this.user = user;
    }

    public User getUser(){
        return user;
    }
}