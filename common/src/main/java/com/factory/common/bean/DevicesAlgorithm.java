package com.factory.common.bean;

import java.util.Date;

public class DevicesAlgorithm {
    /**
     * id
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.162
     */
    private String id;

    /**
     * 名称
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.162
     */
    private String devicesId;

    /**
     * 算法
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.162
     */
    private String algorithmId;

    /**
     * 状态 1正常 2停用 -1删除
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.162
     */
    private Byte status;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.163
     */
    private Date createDate;

    /**
     * id
     */
    public String getId() {
        return id;
    }

    /**
     * id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 名称
     */
    public String getDevicesId() {
        return devicesId;
    }

    /**
     * 名称
     */
    public void setDevicesId(String devicesId) {
        this.devicesId = devicesId == null ? null : devicesId.trim();
    }

    /**
     * 算法
     */
    public String getAlgorithmId() {
        return algorithmId;
    }

    /**
     * 算法
     */
    public void setAlgorithmId(String algorithmId) {
        this.algorithmId = algorithmId == null ? null : algorithmId.trim();
    }

    /**
     * 状态 1正常 2停用 -1删除
     */
    public Byte getStatus() {
        return status;
    }

    /**
     * 状态 1正常 2停用 -1删除
     */
    public void setStatus(Byte status) {
        this.status = status;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}