package com.factory.common.bean;

import java.io.Serializable;
import java.util.Date;

public class User extends BaseBean implements Serializable {
    /**
     * 用户id
     * create by mybatis generator
     * 2020-10-15T15:11:07.476
     */
    private String id;

    /**
     * 账号
     * create by mybatis generator
     * 2020-10-15T15:11:07.476
     */
    private String account;

    /**
     * 密码
     * create by mybatis generator
     * 2020-10-15T15:11:07.476
     */
    private String password;

    /**
     * 用户名称
     * create by mybatis generator
     * 2020-10-15T15:11:07.476
     */
    private String name;

    /**
     * 用户头像
     * create by mybatis generator
     * 2020-10-15T15:11:07.476
     */
    private String profile;

    /**
     * 父id
     * create by mybatis generator
     * 2020-10-15T15:11:07.476
     */
    private String parentId;

    /**
     * 账号绑定的手机号
     * create by mybatis generator
     * 2020-10-15T15:11:07.476
     */
    private String mobile;

    /**
     * 状态1正常 2停用 -1删除
     * create by mybatis generator
     * 2020-10-15T15:11:07.476
     */
    private Integer status;

    /**
     * 创建时间
     * create by mybatis generator
     * 2020-10-15T15:11:07.476
     */
    private Date createDate;

    /**
     * 用户id
     */
    public String getId() {
        return id;
    }

    /**
     * 用户id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 账号
     */
    public String getAccount() {
        return account;
    }

    /**
     * 账号
     */
    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    /**
     * 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 用户名称
     */
    public String getName() {
        return name;
    }

    /**
     * 用户名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 用户头像
     */
    public String getProfile() {
        return profile;
    }

    /**
     * 用户头像
     */
    public void setProfile(String profile) {
        this.profile = profile == null ? null : profile.trim();
    }

    /**
     * 父id
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * 父id
     */
    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    /**
     * 账号绑定的手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 账号绑定的手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 状态1正常 2停用 -1删除
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态1正常 2停用 -1删除
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}