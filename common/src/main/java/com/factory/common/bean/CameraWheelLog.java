package com.factory.common.bean;

import java.util.Date;

public class CameraWheelLog extends BaseBean{
    /**
     * ID
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.987
     */
    private String id;

    /**
     * 巡检编号
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.991
     */
    private String num;

    /**
     * 用户id
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.991
     */
    private String userId;

    /**
     * 用户名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.992
     */
    private String userName;

    /**
     * 位置id
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.992
     */
    private String locationId;

    /**
     * 位置名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.992
     */
    private String locationName;

    /**
     * 摄像头id
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.992
     */
    private String cameraId;

    /**
     * 摄像头名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.992
     */
    private String cameraName;

    /**
     * 名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.992
     */
    private String name;

    /**
     * 1AI巡检 2人工巡检
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.992
     */
    private Integer type;

    /**
     * 巡检结果
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.992
     */
    private String result;

    /**
     * 抓拍照片
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.992
     */
    private String image;

    /**
     * 描述
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.992
     */
    private String description;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.993
     */
    private Date createDate;

    /**
     * 创建者
	 * create by mybatis generator
	 * 2021-06-05T14:26:57.993
     */
    private String createName;

    /**
     * ID
     */
    public String getId() {
        return id;
    }

    /**
     * ID
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 巡检编号
     */
    public String getNum() {
        return num;
    }

    /**
     * 巡检编号
     */
    public void setNum(String num) {
        this.num = num == null ? null : num.trim();
    }

    /**
     * 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 用户名称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户名称
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 位置id
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * 位置id
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId == null ? null : locationId.trim();
    }

    /**
     * 位置名称
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * 位置名称
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName == null ? null : locationName.trim();
    }

    /**
     * 摄像头id
     */
    public String getCameraId() {
        return cameraId;
    }

    /**
     * 摄像头id
     */
    public void setCameraId(String cameraId) {
        this.cameraId = cameraId == null ? null : cameraId.trim();
    }

    /**
     * 摄像头名称
     */
    public String getCameraName() {
        return cameraName;
    }

    /**
     * 摄像头名称
     */
    public void setCameraName(String cameraName) {
        this.cameraName = cameraName == null ? null : cameraName.trim();
    }

    /**
     * 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 1AI巡检 2人工巡检
     */
    public Integer getType() {
        return type;
    }

    /**
     * 1AI巡检 2人工巡检
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 巡检结果
     */
    public String getResult() {
        return result;
    }

    /**
     * 巡检结果
     */
    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }

    /**
     * 抓拍照片
     */
    public String getImage() {
        return image;
    }

    /**
     * 抓拍照片
     */
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    /**
     * 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 描述
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 创建者
     */
    public String getCreateName() {
        return createName;
    }

    /**
     * 创建者
     */
    public void setCreateName(String createName) {
        this.createName = createName == null ? null : createName.trim();
    }
}