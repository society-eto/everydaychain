package com.factory.common.bean;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Turnover extends BaseBean{
    /**
     * 主键
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.010
     */
    private String id;

    /**
     * 所属用ID
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.010
     */
    private String userId;

    /**
     * 所属用户名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.011
     */
    private String userName;

    /**
     * 位置id
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.011
     */
    private String locationId;

    /**
     * 位置名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.011
     */
    private String locationName;

    /**
     * 订单数量
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.011
     */
    private Integer orders;

    /**
     * 销售额
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.011
     */
    private Float sale;

    /**
     * 均价
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.011
     */
    private Float price;

    /**
     * 备注
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.011
     */
    private String remark;

    /**
     * 日期
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.011
     */
    private Date ordersDate;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.011
     */
    private Date createDate;

    /**
     * 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 所属用ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 所属用ID
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 所属用户名称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 所属用户名称
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 位置id
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * 位置id
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId == null ? null : locationId.trim();
    }

    /**
     * 位置名称
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * 位置名称
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName == null ? null : locationName.trim();
    }

    /**
     * 订单数量
     */
    public Integer getOrders() {
        return orders;
    }

    /**
     * 订单数量
     */
    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    /**
     * 销售额
     */
    public Float getSale() {
        return sale;
    }

    /**
     * 销售额
     */
    public void setSale(Float sale) {
        this.sale = sale;
    }

    /**
     * 均价
     */
    public Float getPrice() {
        return price;
    }

    /**
     * 均价
     */
    public void setPrice(Float price) {
        this.price = price;
    }

    /**
     * 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 日期
     */
    public Date getOrdersDate() {
        return ordersDate;
    }

    /**
     * 日期
     */
    public void setOrdersDate(Date ordersDate) {
        this.ordersDate = ordersDate;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}