package com.factory.common.bean;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Date;

public class CloudRadioGroup extends BaseBean {
    /**
     * id
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.827
     */
    private String id;

    /**
     * 绑定用户id
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.827
     */
    private String userId;

    /**
     * 所属用户名
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.827
     */
    private String userName;

    /**
     * 分组名称
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.827
     */
    private String name;

    /**
     * 排序
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.827
     */
    private Integer number;

    /**
     * 状态1正常2停用-1删除
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.827
     */
    private Integer status;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.827
     */
    private Date createDate;

    /**
     * id
     */
    public String getId() {
        return id;
    }

    /**
     * id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 绑定用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 绑定用户id
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 所属用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 所属用户名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 分组名称
     */
    public String getName() {
        return name;
    }

    /**
     * 分组名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 排序
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * 排序
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * 状态1正常2停用-1删除
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态1正常2停用-1删除
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}