package com.factory.common.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class Speech extends BaseBean{
    /**
     * 主键
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.008
     */
    private String id;

    /**
     * 所属设备id
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.008
     */
    private String deviceId;

    /**
     * 所属设备名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.008
     */
    private String deviceName;

    /**
     * 所属用ID
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.008
     */
    private String userId;

    /**
     * 所属用户名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.008
     */
    private String userName;

    /**
     * 位置id
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.009
     */
    private String locationId;

    /**
     * 位置名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.009
     */
    private String locationName;

    /**
     * 识别内容
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.009
     */
    private String speechContent;

    /**
     * 识别类型
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.009
     */
    private Integer speechType;

    /**
     * 识别次数
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.009
     */
    private Integer numbers;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.009
     */
    private Date createDate;

    /** pengpeng
     * base文件上传路径
     */
    private  String basePath;

    /**
     * base文件存储路径
     * @return
     */
    public String getBasePath() {
        return basePath;
    }

    /**
     * base文件存储路径
     * @return
     */
    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    /**
     * 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 所属设备id
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 所属设备id
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId == null ? null : deviceId.trim();
    }

    /**
     * 所属设备名称
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * 所属设备名称
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName == null ? null : deviceName.trim();
    }

    /**
     * 所属用ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 所属用ID
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 所属用户名称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 所属用户名称
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 位置id
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * 位置id
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId == null ? null : locationId.trim();
    }

    /**
     * 位置名称
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * 位置名称
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName == null ? null : locationName.trim();
    }

    /**
     * 识别内容
     */
    public String getSpeechContent() {
        return speechContent;
    }

    /**
     * 识别内容
     */
    public void setSpeechContent(String speechContent) {
        this.speechContent = speechContent == null ? null : speechContent.trim();
    }

    /**
     * 识别类型
     */
    public Integer getSpeechType() {
        return speechType;
    }

    /**
     * 识别类型
     */
    public void setSpeechType(Integer speechType) {
        this.speechType = speechType;
    }

    /**
     * 识别次数
     */
    public Integer getNumbers() {
        return numbers;
    }

    /**
     * 识别次数
     */
    public void setNumbers(Integer numbers) {
        this.numbers = numbers;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}