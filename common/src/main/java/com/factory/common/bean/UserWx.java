package com.factory.common.bean;

import java.util.Date;

public class UserWx {
    /**
     * id
	 * create by mybatis generator
	 * 2020-11-24T16:39:30.730
     */
    private String id;

    /**
     * 绑定用户id
	 * create by mybatis generator
	 * 2020-11-24T16:39:30.734
     */
    private String userId;

    private String openId;

    /**
     * 昵称
	 * create by mybatis generator
	 * 2020-11-24T16:39:30.734
     */
    private String nickname;

    /**
     * 用户头像
	 * create by mybatis generator
	 * 2020-11-24T16:39:30.734
     */
    private String avatarurl;

    /**
     * 性别
	 * create by mybatis generator
	 * 2020-11-24T16:39:30.734
     */
    private Integer gender;

    /**
     * 省份
	 * create by mybatis generator
	 * 2020-11-24T16:39:30.734
     */
    private String province;

    /**
     * 城市
	 * create by mybatis generator
	 * 2020-11-24T16:39:30.734
     */
    private String city;

    /**
     * 国家
	 * create by mybatis generator
	 * 2020-11-24T16:39:30.734
     */
    private String country;

    /**
     * 状态1正常 2停用 -1删除
	 * create by mybatis generator
	 * 2020-11-24T16:39:30.734
     */
    private Integer status;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2020-11-24T16:39:30.734
     */
    private Date createDate;

    /**
     * id
     */
    public String getId() {
        return id;
    }

    /**
     * id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 绑定用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 绑定用户id
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 昵称
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * 昵称
     */
    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    /**
     * 用户头像
     */
    public String getAvatarurl() {
        return avatarurl;
    }

    /**
     * 用户头像
     */
    public void setAvatarurl(String avatarurl) {
        this.avatarurl = avatarurl == null ? null : avatarurl.trim();
    }

    /**
     * 性别
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * 性别
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * 省份
     */
    public String getProvince() {
        return province;
    }

    /**
     * 省份
     */
    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    /**
     * 城市
     */
    public String getCity() {
        return city;
    }

    /**
     * 城市
     */
    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    /**
     * 国家
     */
    public String getCountry() {
        return country;
    }

    /**
     * 国家
     */
    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    /**
     * 状态1正常 2停用 -1删除
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态1正常 2停用 -1删除
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}