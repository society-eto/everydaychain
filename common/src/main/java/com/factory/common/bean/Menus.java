package com.factory.common.bean;

import java.util.Date;

public class Menus {
    /**
     * id
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.168
     */
    private String id;

    /**
     * 菜单名称
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.168
     */
    private String name;

    /**
     * 父id
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.169
     */
    private String parentId;

    /**
     * 菜单路径
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.169
     */
    private String path;

    /**
     * 图标
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.169
     */
    private String icon;

    /**
     * 类型 1管理员菜单 2用户菜单
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.169
     */
    private Integer type;

    /**
     * 排序
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.169
     */
    private Integer sort;

    /**
     * 状态1正常 2停用 -1删除
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.169
     */
    private Integer status;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.169
     */
    private Date createDate;

    /**
     * id
     */
    public String getId() {
        return id;
    }

    /**
     * id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 菜单名称
     */
    public String getName() {
        return name;
    }

    /**
     * 菜单名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 父id
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * 父id
     */
    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    /**
     * 菜单路径
     */
    public String getPath() {
        return path;
    }

    /**
     * 菜单路径
     */
    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }

    /**
     * 图标
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 图标
     */
    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    /**
     * 类型 1管理员菜单 2用户菜单
     */
    public Integer getType() {
        return type;
    }

    /**
     * 类型 1管理员菜单 2用户菜单
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 排序
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * 排序
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 状态1正常 2停用 -1删除
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态1正常 2停用 -1删除
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}