package com.factory.common.bean;

import java.util.Date;

public class Alarm extends BaseBean{
    /**
     * 主键
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.197
     */
    private String id;

    /**
     * 所属用户
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.200
     */
    private String userId;

    /**
     * 用户名称
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.200
     */
    private String userName;

    /**
     * 设备id
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.200
     */
    private String deviceId;

    /**
     * 设备名称
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.200
     */
    private String deviceName;

    /**
     * 摄像头id
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.200
     */
    private String locationId;

    /**
     * 摄像头名称
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.201
     */
    private String locationName;

    /**
     * 摄像头id
     * create by mybatis generator
     * 2020-11-17T15:43:26.333
     */
    private String cameraId;

    /**
     * 摄像头名称
     * create by mybatis generator
     * 2020-11-17T15:43:26.333
     */
    private String cameraName;

    /**
     * 图片地址
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.201
     */
    private String picture;

    /**
     * 状态 -1 删除 1正常
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.201
     */
    private Integer status;

    /**
     * 类型
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.201
     */
    private String type;

    /**
     * 类型名称
     * create by mybatis generator
     * 2020-10-15T14:03:21.201
     */
    private String name;

    /**
     * 描述
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.201
     */
    private String description;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2020-10-15T14:03:21.201
     */
    private Date createDate;

    /**
     * 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 所属用户
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 所属用户
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 用户名称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户名称
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 设备id
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 设备id
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId == null ? null : deviceId.trim();
    }

    /**
     * 设备名称
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * 设备名称
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName == null ? null : deviceName.trim();
    }

    /**
     * 摄像头id
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * 摄像头id
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId == null ? null : locationId.trim();
    }

    /**
     * 摄像头名称
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * 摄像头名称
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName == null ? null : locationName.trim();
    }

    /**
     * 图片地址
     */
    public String getPicture() {
        return picture;
    }

    /**
     * 图片地址
     */
    public void setPicture(String picture) {
        this.picture = picture == null ? null : picture.trim();
    }

    /**
     * 状态 -1 删除 1正常
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态 -1 删除 1正常
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 类型
     */
    public String getType() {
        return type;
    }

    /**
     * 类型
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 描述
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCameraId() {
        return cameraId;
    }

    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}