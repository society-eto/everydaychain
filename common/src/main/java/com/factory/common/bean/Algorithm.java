package com.factory.common.bean;

import java.util.Date;

public class Algorithm extends BaseBean{
    /**
     * 算法id
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.154
     */
    private String id;

    /**
     * 算法名称
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.154
     */
    private String name;

    /**
     * 算法头像
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.154
     */
    private String image;

    /**
     * 算法型号
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.155
     */
    private String model;

    /**
     * 算法版本号
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.155
     */
    private String version;

    /**
     * 算法下载地址
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.155
     */
    private String url;

    /**
     * 算法展示价格单位分
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.155
     */
    private Integer display;

    /**
     * 算法实际价格单位分
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.155
     */
    private Integer price;

    /**
     *
     */
    private String audio;

    /**
     * 算法状态 1正常 2停用 -1删除
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.155
     */
    private Byte status;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.155
     */
    private Date createDate;

    /**
     * 算法id
     */
    public String getId() {
        return id;
    }

    /**
     * 算法id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 算法名称
     */
    public String getName() {
        return name;
    }

    /**
     * 算法名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 算法头像
     */
    public String getImage() {
        return image;
    }

    /**
     * 算法头像
     */
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    /**
     * 算法型号
     */
    public String getModel() {
        return model;
    }

    /**
     * 算法型号
     */
    public void setModel(String model) {
        this.model = model == null ? null : model.trim();
    }

    /**
     * 算法版本号
     */
    public String getVersion() {
        return version;
    }

    /**
     * 算法版本号
     */
    public void setVersion(String version) {
        this.version = version == null ? null : version.trim();
    }

    /**
     * 算法下载地址
     */
    public String getUrl() {
        return url;
    }

    /**
     * 算法下载地址
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * 算法展示价格单位分
     */
    public Integer getDisplay() {
        return display;
    }

    /**
     * 算法展示价格单位分
     */
    public void setDisplay(Integer display) {
        this.display = display;
    }

    /**
     * 算法实际价格单位分
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * 算法实际价格单位分
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * 算法状态 1正常 2停用 -1删除
     */
    public Byte getStatus() {
        return status;
    }

    /**
     * 算法状态 1正常 2停用 -1删除
     */
    public void setStatus(Byte status) {
        this.status = status;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }
}