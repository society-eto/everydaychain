package com.factory.common.bean;

import java.util.Date;

public class MemberFace extends BaseBean{
    /**
     * id
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.974
     */
    private String id;


    /**
     * 所属设备id
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.975
     */
    private String deviceId;

    /**
     * 所属名称
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.975
     */
    private String deviceName;

    /**
     * 所属用户id
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.975
     */
    private String userId;

    /**
     * 所属用户名称
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.975
     */
    private String userName;

    /**
     * 位置id
     * create by mybatis generator
     * 2020-11-10T10:03:56.005
     */
    private String locationId;

    /**
     * 位置名称
     * create by mybatis generator
     * 2020-11-10T10:03:56.005
     */
    private String locationName;


    /**
     * 会员id
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.975
     */
    private String memberId;

    /**
     * 性别 0男 1女 2未知
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.975
     */
    private Integer gender;

    /**
     * 年龄
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.975
     */
    private Integer age;

    /**
     * 图片地址
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.975
     */
    private String imagePath;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.975
     */
    private Date createDate;

    /**
     * 人脸特征值
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.975
     */
    private String faceValue;

    /**
     * id
     */
    public String getId() {
        return id;
    }

    /**
     * id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }


    /**
     * 所属设备id
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 所属设备id
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId == null ? null : deviceId.trim();
    }

    /**
     * 所属名称
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * 所属名称
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName == null ? null : deviceName.trim();
    }

    /**
     * 所属用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 所属用户id
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 所属用户名称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 所属用户名称
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 位置id
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * 位置id
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId == null ? null : locationId.trim();
    }

    /**
     * 位置名称
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * 位置名称
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName == null ? null : locationName.trim();
    }

    /**
     * 会员id
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * 会员id
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    /**
     * 性别 0男 1女 2未知
     */
    public Integer getGander() {
        return gender;
    }

    /**
     * 性别 0男 1女 2未知
     */
    public void setGander(Integer gender) {
        this.gender = gender;
    }

    /**
     * 年龄
     */
    public Integer getAge() {
        return age;
    }

    /**
     * 年龄
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * 图片地址
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * 图片地址
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath == null ? null : imagePath.trim();
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 人脸特征值
     */
    public String getFaceValue() {
        return faceValue;
    }

    /**
     * 人脸特征值
     */
    public void setFaceValue(String faceValue) {
        this.faceValue = faceValue == null ? null : faceValue.trim();
    }
}