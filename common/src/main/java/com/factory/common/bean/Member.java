package com.factory.common.bean;

import java.util.Date;

public class Member extends BaseBean{
    /**
     * 主键
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.972
     */
    private String id;


    /**
     * 所属用户id
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.972
     */
    private String userId;

    /**
     * 所属用户名称
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private String userName;

    /**
     * 姓名
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private String name;

    /**
     * 性别 1男 2女
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private Byte gender;

    /**
     * 年龄
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private Integer age;

    /**
     * 身份号
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private String idcard;

    /**
     * 标签
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private String label;

    /**
     * 身份证正面
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private String photo;

    /**
     * 省份证反面
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private String photo1;

    /**
     * 人脸照片
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private String photo2;

    /**
     * 备注
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private String remarks;

    /**
     * 所属nanoID
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private String deviceId;

    /**
     * 所属Nano名称
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private String deviceName;

    /**
     * 状态 1 正常 -1 删除
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private Boolean status;

    /**
     * 更新时间
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private Date updateDate;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.973
     */
    private Date createDate;

    /**
     * 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }


    /**
     * 所属用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 所属用户id
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 所属用户名称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 所属用户名称
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 性别 1男 2女
     */
    public Byte getGender() {
        return gender;
    }

    /**
     * 性别 1男 2女
     */
    public void setGender(Byte gender) {
        this.gender = gender;
    }

    /**
     * 年龄
     */
    public Integer getAge() {
        return age;
    }

    /**
     * 年龄
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * 身份号
     */
    public String getIdcard() {
        return idcard;
    }

    /**
     * 身份号
     */
    public void setIdcard(String idcard) {
        this.idcard = idcard == null ? null : idcard.trim();
    }

    /**
     * 标签
     */
    public String getLabel() {
        return label;
    }

    /**
     * 标签
     */
    public void setLabel(String label) {
        this.label = label == null ? null : label.trim();
    }

    /**
     * 身份证正面
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 身份证正面
     */
    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    /**
     * 省份证反面
     */
    public String getPhoto1() {
        return photo1;
    }

    /**
     * 省份证反面
     */
    public void setPhoto1(String photo1) {
        this.photo1 = photo1 == null ? null : photo1.trim();
    }

    /**
     * 人脸照片
     */
    public String getPhoto2() {
        return photo2;
    }

    /**
     * 人脸照片
     */
    public void setPhoto2(String photo2) {
        this.photo2 = photo2 == null ? null : photo2.trim();
    }

    /**
     * 备注
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * 备注
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    /**
     * 所属nanoID
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 所属nanoID
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId == null ? null : deviceId.trim();
    }

    /**
     * 所属Nano名称
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * 所属Nano名称
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName == null ? null : deviceName.trim();
    }

    /**
     * 状态 1 正常 -1 删除
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 状态 1 正常 -1 删除
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 更新时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}