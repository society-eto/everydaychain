package com.factory.common.bean;

import lombok.Data;

import java.util.Date;

@Data
public class Camera extends BaseBean{
    /**
     * 数据id
     * create by mybatis generator
     * 2020-11-17T15:46:35.623
     */
    private String id;

    /**
     * 所属设备id
     * create by mybatis generator
     * 2020-11-17T15:46:35.626
     */
    private String deviceId;

    /**
     * 所属设备名称
     * create by mybatis generator
     * 2020-11-17T15:43:26.333
     */
    private String deviceName;

    /**
     * 所属用户id
     * create by mybatis generator
     * 2020-11-17T15:43:26.333
     */
    private String userId;

    /**
     * 所用用户名
     * create by mybatis generator
     * 2020-11-17T15:43:26.333
     */
    private String userName;

    /**
     * 位置id
     * create by mybatis generator
     * 2020-11-17T15:43:26.333
     */
    private String locationId;

    /**
     * 位置名称
     * create by mybatis generator
     * 2020-11-17T15:43:26.333
     */
    private String locationName;

    /**
     * 设备名称
     * create by mybatis generator
     * 2020-11-17T15:43:26.345
     */
    private String name;

    /**
     * 摄像头ip
     * create by mybatis generator
     * 2020-11-17T15:43:26.346
     */
    private String ip;

    /**
     * 摄像头账号
     * create by mybatis generator
     * 2020-11-17T15:43:26.346
     */
    private String account;

    /**
     * 摄像头密码
     * create by mybatis generator
     * 2020-11-17T15:43:26.346
     */
    private String password;

    /**
     * 端口号
     * create by mybatis generator
     * 2020-11-17T15:43:26.346
     */
    private String port;

    /**
     * 渠道号
     * create by mybatis generator
     * 2020-11-17T15:43:26.346
     */
    private String channel;

    /**
     * 设备类型1海康2大华
     * create by mybatis generator
     * 2020-11-17T15:43:26.346
     */
    private Integer type;

    /**
     * 功能数据
     * create by mybatis generator
     * 2020-11-17T15:43:26.346
     */
    private String functions;//varchar(1024) NULL功能数据

    /**
     * 绘制数据
     * create by mybatis generator
     * 2020-11-17T15:43:26.346
     */
    private String drawData;// varchar(1024) NULL绘制数据

    /**
     * 告警手机号
     * create by mybatis generator
     * 2020-11-17T15:43:26.346
     */
    private String mobile;// varchar(1024) NULL告警手机号

    /**
     * -1删除2关闭状态1开启状态
     * create by mybatis generator
     * 2020-11-17T15:43:26.346
     */
    private Integer status;

    /**
     * 创建时间
     * create by mybatis generator
     * 2020-11-17T15:43:26.346
     */
    private Date createDate;

    @Override
    public String toString() {
        return "Camera{" +
                "id='" + id + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", locationId='" + locationId + '\'' +
                ", locationName='" + locationName + '\'' +
                ", name='" + name + '\'' +
                ", ip='" + ip + '\'' +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", port='" + port + '\'' +
                ", type=" + type +
                ", functions='" + functions + '\'' +
                ", drawData='" + drawData + '\'' +
                ", mobile='" + mobile + '\'' +
                ", status=" + status +
                ", createDate=" + createDate +
                '}';
    }
}