package com.factory.common.bean;

import java.util.Date;

public class Customers extends BaseBean{
    /**
     * 主键
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.957
     */
    private String id;

    /**
     * 所属设备id
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.960
     */
    private String deviceId;

    /**
     * 所属设备名称
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.960
     */
    private String deviceName;

    /**
     * 所属用ID
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.960
     */
    private String userId;

    /**
     * 所属用户名称
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.960
     */
    private String userName;

    /**
     * 位置id
     * create by mybatis generator
     * 2020-11-10T10:03:56.005
     */
    private String locationId;

    /**
     * 位置名称
     * create by mybatis generator
     * 2020-11-10T10:03:56.005
     */
    private String locationName;

    /**
     * 人脸特征id
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.961
     */
    private String memberFaceId;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 性别
     */
    private  Integer gender;

    /**
     * 客户类型
     */
    private Integer customersType;


    /**
     * 创建时间
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.961
     */
    private Date createDate;

    /**
     * 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }


    /**
     * 所属设备id
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 所属设备id
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId == null ? null : deviceId.trim();
    }

    /**
     * 所属设备名称
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * 所属设备名称
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName == null ? null : deviceName.trim();
    }

    /**
     * 所属用ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 所属用ID
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 所属用户名称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 所属用户名称
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 位置id
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * 位置id
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId == null ? null : locationId.trim();
    }

    /**
     * 位置名称
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * 位置名称
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName == null ? null : locationName.trim();
    }

    /**
     * 人脸特征id
     */
    public String getMemberFaceId() {
        return memberFaceId;
    }

    /**
     * 人脸特征id
     */
    public void setMemberFaceId(String memberFaceId) {
        this.memberFaceId = memberFaceId == null ? null : memberFaceId.trim();
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getCustomersType() {
        return customersType;
    }

    public void setCustomersType(Integer customersType) {
        this.customersType = customersType;
    }
}