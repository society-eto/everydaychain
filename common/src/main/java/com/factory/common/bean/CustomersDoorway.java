package com.factory.common.bean;

import java.util.Date;

public class CustomersDoorway extends BaseBean{
    /**
     * 主键
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.006
     */
    private String id;

    /**
     * 所属设备id
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.006
     */
    private String deviceId;

    /**
     * 所属设备名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.006
     */
    private String deviceName;

    /**
     * 所属用ID
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.006
     */
    private String userId;

    /**
     * 所属用户名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.006
     */
    private String userName;

    /**
     * 位置id
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.007
     */
    private String locationId;

    /**
     * 位置名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.007
     */
    private String locationName;

    /**
     * 位置id
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.007
     */
    private String cameraId;

    /**
     * 位置名称
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.007
     */
    private String cameraName;

    /**
     * 客流数量
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.007
     */
    private Integer numbers;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2021-06-05T14:26:58.007
     */
    private Date createDate;

    /**
     * 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 所属设备id
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 所属设备id
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId == null ? null : deviceId.trim();
    }

    /**
     * 所属设备名称
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * 所属设备名称
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName == null ? null : deviceName.trim();
    }

    /**
     * 所属用ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 所属用ID
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 所属用户名称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 所属用户名称
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 位置id
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * 位置id
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId == null ? null : locationId.trim();
    }

    /**
     * 位置名称
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * 位置名称
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName == null ? null : locationName.trim();
    }

    /**
     * 位置id
     */
    public String getCameraId() {
        return cameraId;
    }

    /**
     * 位置id
     */
    public void setCameraId(String cameraId) {
        this.cameraId = cameraId == null ? null : cameraId.trim();
    }

    /**
     * 位置名称
     */
    public String getCameraName() {
        return cameraName;
    }

    /**
     * 位置名称
     */
    public void setCameraName(String cameraName) {
        this.cameraName = cameraName == null ? null : cameraName.trim();
    }

    /**
     * 客流数量
     */
    public Integer getNumbers() {
        return numbers;
    }

    /**
     * 客流数量
     */
    public void setNumbers(Integer numbers) {
        this.numbers = numbers;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}