package com.factory.common.bean;

import java.io.Serializable;
import java.util.Date;

public class UserDevices extends BaseBean implements Serializable {
    /**
     * id
     * create by mybatis generator
     * 2020-11-10T09:57:50.541
     */
    private String id;

    /**
     * 设备名称
     * create by mybatis generator
     * 2020-11-10T09:57:50.546
     */
    private String name;

    /**
     * 设备头像
     * create by mybatis generator
     * 2020-11-10T09:57:50.546
     */
    private String image;

    /**
     * 设备型号
     * create by mybatis generator
     * 2020-11-10T09:57:50.546
     */
    private String model;

    /**
     * 设备版本号
     * create by mybatis generator
     * 2020-11-10T09:57:50.546
     */
    private String version;

    /**
     * 本地ip
     * create by mybatis generator
     * 2020-11-10T09:57:50.546
     */
    private String localIp;

    /**
     * 过期时间
     * create by mybatis generator
     * 2020-11-10T09:57:50.546
     */
    private Date deadline;

    /**
     * 用户id
     * create by mybatis generator
     * 2020-11-10T09:57:50.546
     */
    private String userId;

    /**
     * 用户名称
     * create by mybatis generator
     * 2020-11-10T09:57:50.550
     */
    private String userName;

    /**
     * 位置id
     * create by mybatis generator
     * 2020-11-10T09:57:50.550
     */
    private String locationId;

    /**
     * 位置名称
     * create by mybatis generator
     * 2020-11-10T09:57:50.550
     */
    private String locationName;

    /**
     * 盒子访问二级域名
     */
    private String domain;

    /**
     * 状态 1正常 2未激活 3过期 -1删除
     * create by mybatis generator
     * 2020-11-10T09:57:50.550
     */
    private Integer status;

    /**
     * 创建时间
     * create by mybatis generator
     * 2020-11-10T09:57:50.550
     */
    private Date createDate;

    /**
     * id
     */
    public String getId() {
        return id;
    }

    /**
     * id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 设备名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设备名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 设备头像
     */
    public String getImage() {
        return image;
    }

    /**
     * 设备头像
     */
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    /**
     * 设备型号
     */
    public String getModel() {
        return model;
    }

    /**
     * 设备型号
     */
    public void setModel(String model) {
        this.model = model == null ? null : model.trim();
    }

    /**
     * 设备版本号
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设备版本号
     */
    public void setVersion(String version) {
        this.version = version == null ? null : version.trim();
    }

    /**
     * 本地ip
     */
    public String getLocalIp() {
        return localIp;
    }

    /**
     * 本地ip
     */
    public void setLocalIp(String localIp) {
        this.localIp = localIp == null ? null : localIp.trim();
    }

    /**
     * 过期时间
     */
    public Date getDeadline() {
        return deadline;
    }

    /**
     * 过期时间
     */
    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    /**
     * 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 用户名称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户名称
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 位置id
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * 位置id
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId == null ? null : locationId.trim();
    }

    /**
     * 位置名称
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * 位置名称
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName == null ? null : locationName.trim();
    }

    /**
     * 状态 1正常 2未激活 3过期 -1删除
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态 1正常 2未激活 3过期 -1删除
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }
}