package com.factory.common.bean;

import java.io.Serializable;
import java.util.Date;

public class Manager extends BaseBean implements Serializable {
    /**
     * 管理员id
     * create by mybatis generator
     * 2020-10-15T15:11:07.477
     */
    private String id;

    /**
     * 账号
     * create by mybatis generator
     * 2020-10-15T15:11:07.477
     */
    private String account;

    /**
     * 密码
     * create by mybatis generator
     * 2020-10-15T15:11:07.477
     */
    private String password;

    /**
     * 名称
     * create by mybatis generator
     * 2020-10-15T15:11:07.477
     */
    private String name;

    /**
     * 头像
     * create by mybatis generator
     * 2020-10-15T15:11:07.477
     */
    private String profile;

    /**
     * 父id
     * create by mybatis generator
     * 2020-10-15T15:11:07.477
     */
    private String parentId;

    /**
     * 账号绑定的手机号
     * create by mybatis generator
     * 2020-10-15T15:11:07.477
     */
    private String mobile;

    /**
     * 状态1正常2停用-1删除
     * create by mybatis generator
     * 2020-10-15T15:11:07.477
     */
    private Byte status;

    /**
     * 创建时间
     * create by mybatis generator
     * 2020-10-15T15:11:07.477
     */
    private Date createDate;

    /**
     * 角色
     */
    private Integer role;

    /**
     * 管理员id
     */
    public String getId() {
        return id;
    }

    /**
     * 管理员id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 账号
     */
    public String getAccount() {
        return account;
    }

    /**
     * 账号
     */
    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    /**
     * 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 头像
     */
    public String getProfile() {
        return profile;
    }

    /**
     * 头像
     */
    public void setProfile(String profile) {
        this.profile = profile == null ? null : profile.trim();
    }

    /**
     * 父id
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * 父id
     */
    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    /**
     * 账号绑定的手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 账号绑定的手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 状态1正常2停用-1删除
     */
    public Byte getStatus() {
        return status;
    }

    /**
     * 状态1正常2停用-1删除
     */
    public void setStatus(Byte status) {
        this.status = status;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }
}