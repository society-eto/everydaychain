package com.factory.common.bean;

import java.util.Date;

public class Devices extends BaseBean{
    /**
     * 设备id
     * create by mybatis generator
     * 2020-11-04T10:24:13.872
     */
    private String id;

    /**
     * 设备名称
     * create by mybatis generator
     * 2020-11-04T10:24:13.872
     */
    private String name;

    /**
     * 设备头像
     * create by mybatis generator
     * 2020-11-04T10:24:13.872
     */
    private String image;

    /**
     * 设备型号
     * create by mybatis generator
     * 2020-11-04T10:24:13.872
     */
    private String model;

    /**
     * 设备版本号
     * create by mybatis generator
     * 2020-11-04T10:24:13.872
     */
    private String version;

    /**
     * 设备展示价格单位分
     * create by mybatis generator
     * 2020-11-04T10:24:13.872
     */
    private Integer display;

    /**
     * 设备实际价格单位分
     * create by mybatis generator
     * 2020-11-04T10:24:13.872
     */
    private Integer price;

    /**
     * 状态 1展示 2不展示
     * create by mybatis generator
     * 2020-11-04T10:24:13.872
     */
    private Integer isShow;

    /**
     * 状态 1正常 2未激活 3过期 -1删除
     * create by mybatis generator
     * 2020-11-04T10:24:13.872
     */
    private Integer status;

    /**
     * 更新时间
     * create by mybatis generator
     * 2020-11-04T10:24:13.872
     */
    private Date updateDate;

    /**
     * 创建时间
     * create by mybatis generator
     * 2020-11-04T10:24:13.872
     */
    private Date createDate;

    /**
     * 产品描述
     * create by mybatis generator
     * 2020-11-04T10:24:13.882
     */
    private String description;

    /**
     * 设备id
     */
    public String getId() {
        return id;
    }

    /**
     * 设备id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 设备名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设备名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 设备头像
     */
    public String getImage() {
        return image;
    }

    /**
     * 设备头像
     */
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    /**
     * 设备型号
     */
    public String getModel() {
        return model;
    }

    /**
     * 设备型号
     */
    public void setModel(String model) {
        this.model = model == null ? null : model.trim();
    }

    /**
     * 设备版本号
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设备版本号
     */
    public void setVersion(String version) {
        this.version = version == null ? null : version.trim();
    }

    /**
     * 设备展示价格单位分
     */
    public Integer getDisplay() {
        return display;
    }

    /**
     * 设备展示价格单位分
     */
    public void setDisplay(Integer display) {
        this.display = display;
    }

    /**
     * 设备实际价格单位分
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * 设备实际价格单位分
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * 状态 1正常 2未激活 3过期 -1删除
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态 1正常 2未激活 3过期 -1删除
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 更新时间
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 更新时间
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 产品描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 产品描述
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }
}