package com.factory.common.bean;

import java.util.Date;

public class CloudRadio extends BaseBean{
    /**
     * id
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.810
     */
    private String id;

    /**
     * 绑定用户id
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.813
     */
    private String userId;

    /**
     * 所属用户名
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.813
     */
    private String userName;

    /**
     * 分组id
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.814
     */
    private String cloudRadioGroupId;

    /**
     * 分组名称
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.814
     */
    private String cloudRadioGroupName;

    /**
     * 广播名称
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.814
     */
    private String name;

    /**
     * 文件地址
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.814
     */
    private String path;

    /**
     * 文件类型
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.814
     */
    private String type;

    /**
     * 文件时长
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.814
     */
    private Integer times;

    /**
     * 排序
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.814
     */
    private Integer number;

    /**
     * 状态1正常2停用-1删除
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.814
     */
    private Integer status;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.814
     */
    private Date createDate;

    /**
     * id
     */
    public String getId() {
        return id;
    }

    /**
     * id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 绑定用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 绑定用户id
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 所属用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 所属用户名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 分组id
     */
    public String getCloudRadioGroupId() {
        return cloudRadioGroupId;
    }

    /**
     * 分组id
     */
    public void setCloudRadioGroupId(String cloudRadioGroupId) {
        this.cloudRadioGroupId = cloudRadioGroupId == null ? null : cloudRadioGroupId.trim();
    }

    /**
     * 分组名称
     */
    public String getCloudRadioGroupName() {
        return cloudRadioGroupName;
    }

    /**
     * 分组名称
     */
    public void setCloudRadioGroupName(String cloudRadioGroupName) {
        this.cloudRadioGroupName = cloudRadioGroupName == null ? null : cloudRadioGroupName.trim();
    }

    /**
     * 广播名称
     */
    public String getName() {
        return name;
    }

    /**
     * 广播名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 文件地址
     */
    public String getPath() {
        return path;
    }

    /**
     * 文件地址
     */
    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }

    /**
     * 文件类型
     */
    public String getType() {
        return type;
    }

    /**
     * 文件类型
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 文件时长
     */
    public Integer getTimes() {
        return times;
    }

    /**
     * 文件时长
     */
    public void setTimes(Integer times) {
        this.times = times;
    }

    /**
     * 排序
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * 排序
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * 状态1正常2停用-1删除
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态1正常2停用-1删除
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}