package com.factory.common.bean;

import java.util.Date;

public class UserMenus {
    /**
     * id
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.178
     */
    private String id;

    /**
     * 所属用户户id
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.178
     */
    private Integer userId;

    /**
     * 菜单id
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.178
     */
    private String menusId;

    /**
     * 状态1正常 2停用 -1删除
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.178
     */
    private Integer status;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2020-10-15T09:58:27.178
     */
    private Date createDate;

    /**
     * id
     */
    public String getId() {
        return id;
    }

    /**
     * id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 所属用户户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 所属用户户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 菜单id
     */
    public String getMenusId() {
        return menusId;
    }

    /**
     * 菜单id
     */
    public void setMenusId(String menusId) {
        this.menusId = menusId == null ? null : menusId.trim();
    }

    /**
     * 状态1正常 2停用 -1删除
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态1正常 2停用 -1删除
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}