package com.factory.common.bean;

import lombok.Data;

import java.util.Date;

@Data
public class Config extends BaseBean{
    /**
     * id
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.825
     */
    private String id;

    /**
     * 绑定用户id
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.825
     */
    private String userId;


    /**
     * 键
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.826
     */
    private String ckey;

    /**
     * 广播名称
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.826
     */
    private String name;

    /**
     * 值
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.826
     */
    private String cvalue;


    /**
     * 状态1正常2停用-1删除
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.826
     */
    private Integer status;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2021-05-18T14:00:34.826
     */
    private Date createDate;

}