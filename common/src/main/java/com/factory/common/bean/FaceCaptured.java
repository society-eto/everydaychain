package com.factory.common.bean;

import java.util.Date;

public class FaceCaptured extends BaseBean{
    /**
     * 主键
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.957
     */
    private String id;

    /**
     * 所属设备id
	 * create by mybatis generator
	 * 2020-10-21T15:19:14.960
     */
    private String deviceId;

    /**
     * 所属设备名称
     * create by mybatis generator
     * 2021-01-11T09:19:40.210
     */
    private String deviceName;

    /**
     * 所属用ID
     * create by mybatis generator
     * 2021-01-11T09:19:40.210
     */
    private String userId;

    /**
     * 所属用户名称
     * create by mybatis generator
     * 2021-01-11T09:19:40.210
     */
    private String userName;

    /**
     * 位置id
     * create by mybatis generator
     * 2021-01-11T09:19:40.220
     */
    private String locationId;

    /**
     * 位置名称
     * create by mybatis generator
     * 2021-01-11T09:19:40.220
     */
    private String locationName;

    /**
     * 摄像头id
     * create by mybatis generator
     * 2021-01-11T09:19:40.220
     */
    private String cameraId;

    /**
     * 摄像头名称
     * create by mybatis generator
     * 2021-01-11T09:19:40.220
     */
    private String cameraName;

    /**
     * 人脸特征值id
     * create by mybatis generator
     * 2021-01-11T09:19:40.220
     */
    private String faceId;

    /**
     * 人脸
     * create by mybatis generator
     * 2021-01-11T09:19:40.220
     */
    private String faceImage;

    /**
     * 含背景图片
     * create by mybatis generator
     * 2021-01-11T09:19:40.220
     */
    private String image;

    /**
     * 备注1
     * create by mybatis generator
     * 2021-01-11T09:19:40.220
     */
    private String remark1;

    /**
     * 备注2
     * create by mybatis generator
     * 2021-01-11T09:19:40.220
     */
    private String remark2;

    /**
     * 创建时间
     * create by mybatis generator
     * 2021-01-11T09:19:40.220
     */
    private Date createDate;

    /**
     * 所属设备名称
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * 所属设备名称
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName == null ? null : deviceName.trim();
    }

    /**
     * 所属用ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 所属用ID
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 所属用户名称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 所属用户名称
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 位置id
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * 位置id
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId == null ? null : locationId.trim();
    }

    /**
     * 位置名称
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * 位置名称
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName == null ? null : locationName.trim();
    }

    /**
     * 摄像头id
     */
    public String getCameraId() {
        return cameraId;
    }

    /**
     * 摄像头id
     */
    public void setCameraId(String cameraId) {
        this.cameraId = cameraId == null ? null : cameraId.trim();
    }

    /**
     * 摄像头名称
     */
    public String getCameraName() {
        return cameraName;
    }

    /**
     * 摄像头名称
     */
    public void setCameraName(String cameraName) {
        this.cameraName = cameraName == null ? null : cameraName.trim();
    }

    /**
     * 人脸特征值id
     */
    public String getFaceId() {
        return faceId;
    }

    /**
     * 人脸特征值id
     */
    public void setFaceId(String faceId) {
        this.faceId = faceId == null ? null : faceId.trim();
    }

    /**
     * 人脸
     */
    public String getFaceImage() {
        return faceImage;
    }

    /**
     * 人脸
     */
    public void setFaceImage(String faceImage) {
        this.faceImage = faceImage == null ? null : faceImage.trim();
    }

    /**
     * 含背景图片
     */
    public String getImage() {
        return image;
    }

    /**
     * 含背景图片
     */
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    /**
     * 备注1
     */
    public String getRemark1() {
        return remark1;
    }

    /**
     * 备注1
     */
    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    /**
     * 备注2
     */
    public String getRemark2() {
        return remark2;
    }

    /**
     * 备注2
     */
    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }


    /**
     * 所属设备id
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 所属设备id
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId == null ? null : deviceId.trim();
    }
}