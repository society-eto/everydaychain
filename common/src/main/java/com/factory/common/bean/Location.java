package com.factory.common.bean;

import java.util.Date;

public class Location extends BaseBean{
    /**
     * id
	 * create by mybatis generator
	 * 2020-11-10T09:23:43.688
     */
    private String id;

    /**
     * 用户id
     * create by mybatis generator
     * 2020-11-10T09:57:50.546
     */
    private String userId;

    /**
     * 用户名称
     * create by mybatis generator
     * 2020-11-10T09:57:50.550
     */
    private String userName;

    /**
     * 位置名称
	 * create by mybatis generator
	 * 2020-11-10T09:23:43.692
     */
    private String name;

    /**
     * 位置
	 * create by mybatis generator
	 * 2020-11-10T09:23:43.692
     */
    private String address;

    /**
     * 经度
	 * create by mybatis generator
	 * 2020-11-10T09:23:43.692
     */
    private Float lng;

    /**
     * 纬度
	 * create by mybatis generator
	 * 2020-11-10T09:23:43.692
     */
    private Float lat;

    /**
     * 状态1正常2未完成-1删除
	 * create by mybatis generator
	 * 2020-11-10T09:23:43.692
     */
    private Integer status;

    /**
     * 创建时间
	 * create by mybatis generator
	 * 2020-11-10T09:23:43.692
     */
    private Date createDate;

    /**
     * id
     */
    public String getId() {
        return id;
    }

    /**
     * id
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 用户id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 用户id
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 用户名称
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户名称
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * 位置名称
     */
    public String getName() {
        return name;
    }

    /**
     * 位置名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 位置
     */
    public String getAddress() {
        return address;
    }

    /**
     * 位置
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 经度
     */
    public Float getLng() {
        return lng;
    }

    /**
     * 经度
     */
    public void setLng(Float lng) {
        this.lng = lng;
    }

    /**
     * 纬度
     */
    public Float getLat() {
        return lat;
    }

    /**
     * 纬度
     */
    public void setLat(Float lat) {
        this.lat = lat;
    }

    /**
     * 状态1正常2未完成-1删除
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态1正常2未完成-1删除
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}