package com.factory.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MathUtil {


    public static Double mul(Double d1,int i){
        BigDecimal b1 = new BigDecimal(d1.toString());
        BigDecimal b2 = new BigDecimal(String.valueOf(i));
        return b1.multiply(b2).doubleValue();
    }

    /**
     * @Description 两个Double数相除
     * @return Double
     * @author liangbao.huang
     * @date Jul 7, 2014 10:06:56 AM
     */
    public static Double div(Double d1,int i){
        BigDecimal b1 = new BigDecimal(d1.toString());
        BigDecimal b2 = new BigDecimal(String.valueOf(i));
        int DEF_DIV_SCALE = 10;
        return b1.divide(b2,DEF_DIV_SCALE, RoundingMode.HALF_UP).doubleValue();
    }
}
