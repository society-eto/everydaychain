package com.factory.common.utils;

import java.util.Base64;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

	/**
	 * 生成UUID @Title: getUUID @Description: @param: @return @return: String @throws
	 */
	public static String getUUID() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/**
	 * 生成id
	 * @return string
	 */
	public static String getID(){
		return  Long.toHexString(System.currentTimeMillis() / 1000) + randomStr(3);
	}

	/**
	 * 判断是不是空字符串 @Title: notEmpty @Description: @param: @param
	 * string @param: @return @return: boolean @throws
	 */
	public static boolean notEmpty(String string) {
		return null != string && !"".equals(string);
	}

	public static boolean notEmpty(Object string) {
		return null != string && !"".equals(string.toString());
	}

	/**
	 * 判断空字符串
	 *
	 * @param string
	 * @return
	 */
	public static boolean isEmpty(Object string) {
		if(null == string) return true;

		return !notEmpty(string.toString());
	}

	/**
	 * 获取随机字符串
	 * 
	 * @param string
	 * @return
	 */
	public static boolean isEmpty(String string) {
		return !notEmpty(string);
	}

	public static String randomStr(int length) {

		String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";

		Random random1 = new Random();
		// 指定字符串长度，拼接字符并toString
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			// 获取指定长度的字符串中任意一个字符的索引值
			int number = random1.nextInt(str.length());
			// 根据索引值获取对应的字符
			char charAt = str.charAt(number);
			sb.append(charAt);
		}
		return sb.toString();
	}

	public static String randomStr2(int length) {

		String str = "abcdefghijklmnopqrstuvwxyz0123456789";

		Random random1 = new Random();
		// 指定字符串长度，拼接字符并toString
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			// 获取指定长度的字符串中任意一个字符的索引值
			int number = random1.nextInt(str.length());
			// 根据索引值获取对应的字符
			char charAt = str.charAt(number);
			sb.append(charAt);
		}
		return sb.toString();
	}

	/**
	 * 校验正则匹配
	 * @param value
	 * @param regex
	 * @return
	 */
	public static boolean matches(Object value, String regex) {
		if (isEmpty(value)) {
			return false;
		}
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(value.toString());
		return m.matches();
	}

	/**
	 * 转换为整数类型
	 * @param val
	 * @param def
	 * @return
	 */
	public static Integer toInteger(Object val,Integer def){
		if(matches(val,"^\\d+$")){
			return Integer.valueOf(val.toString());
		}
		return def;
	}

	/**
	 * 转为字符串
	 * @param val
	 * @param def
	 * @return
	 */
	public static String val(Object val, String def){
		return isEmpty(val) ? def : val.toString();
	}

	/**
	 * 将驼峰式命名的字符串转换为下划线小写方式。如果转换前的驼峰式命名的字符串为空，则返回空字符串。</br>
	 * 例如：HelloWorld->HELLO_WORLD
	 * @param name 转换前的驼峰式命名的字符串
	 * @return 转换后下划线大写方式命名的字符串
	 */
	public static String underscoreName(String name) {
		StringBuilder result = new StringBuilder();
		if (name != null && name.length() > 0) {
			// 将第一个字符处理成小写
			result.append(name.substring(0, 1).toLowerCase());
			// 循环处理其余字符
			for (int i = 1; i < name.length(); i++) {
				String s = name.substring(i, i + 1);
				// 在大写字母前添加下划线
				if (s.equals(s.toUpperCase()) && !Character.isDigit(s.charAt(0))) {
					result.append("_");
				}
				// 其他字符直接转成小写
				result.append(s.toLowerCase());
			}
		}
		return result.toString();
	}

	/**
	 * 将下划线小写方式命名的字符串转换为驼峰式。如果转换前的下划线大写方式命名的字符串为空，则返回空字符串。</br>
	 * 例如：HELLO_WORLD->HelloWorld
	 * @param name 转换前的下划线大写方式命名的字符串
	 * @return 转换后的驼峰式命名的字符串
	 */
	public static String camelName(String name) {
		StringBuilder result = new StringBuilder();
		// 快速检查
		if (name == null || name.isEmpty()) {
			// 没必要转换
			return "";
		} else if (!name.contains("_")) {
			// 不含下划线，仅将首字母小写
			return name.substring(0, 1).toLowerCase() + name.substring(1);
		}
		// 用下划线将原始字符串分割
		String camels[] = name.split("_");
		for (String camel :  camels) {
			// 跳过原始字符串中开头、结尾的下换线或双重下划线
			if (camel.isEmpty()) {
				continue;
			}
			// 处理真正的驼峰片段
			if (result.length() == 0) {
				// 第一个驼峰片段，全部字母都小写
				result.append(camel.toLowerCase());
			} else {
				// 其他的驼峰片段，首字母大写
				result.append(camel.substring(0, 1).toUpperCase());
				result.append(camel.substring(1).toLowerCase());
			}
		}
		return result.toString();
	}

	/**
	 * 获取base64的字符串
	 * @param str
	 * @return
	 */
	public static String getBase64Str(String str){
		return new String(Base64.getEncoder().encode(str.getBytes()));
	}

  	public static void main(String[] args) {
	  System.out.println(randomStr2(9));
 	}
}
