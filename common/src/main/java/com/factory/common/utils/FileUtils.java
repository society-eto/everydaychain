package com.factory.common.utils;

import java.io.File;

/**
 * 文件操作工具类
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/3 16:20
 */
public class FileUtils {

    /**
     * 移动文件
     *
     * @param src
     * @param targetPath
     * @return
     */
    public static String move(String src, String targetPath) {
        File startFile = new File(src);

        File path = new File(targetPath);
        if(!path.exists()){
            path.mkdirs();
        }

        if (startFile.exists() && startFile.renameTo(new File(targetPath + File.separator + startFile.getName()))) {
            return startFile.getName();
        }

        return null;
    }

}
