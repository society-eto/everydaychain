package com.factory.common.utils;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SmsUtils {

    //产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";
    //LTAI4Fmia6n69ekLp14tWsq2
    static final String accessKeyId = "LTAI4Fmia6n69ekLp14tWsq2";
    //L7PcOA7ifUl2BfNPR1IMROy61rsipB
    static final String accessKeySecret = "L7PcOA7ifUl2BfNPR1IMROy61rsipB";

 /*   public static void main(String[] args) {
        long time = System.currentTimeMillis();
        sendCode("1111","13636703874");
        //sendSms("a","factory-test1","未带安全帽","2020-04-15","13636703874");
    }*/

    /**
     * 发送验证码
     * @param code 验证码
     * @param phone 手机号
     * @return
     */
    public static SendSmsResponse sendCode(String code, String phone){
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        try {
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        } catch (ClientException e) {
            log.error("创建阿里云短息profile,{}",e);
        }
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(phone);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName("智能边缘");
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode("SMS_183260810");
        JSONObject param = new JSONObject();
        param.put("code", code);

        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam(JSONObject.toJSONString(param));
        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = null;
        try {
            sendSmsResponse = acsClient.getAcsResponse(request);
        } catch (ServerException e) {
            log.error("{}",e);
        } catch (ClientException e) {
            log.error("{}",e);
        }

        log.info("阿里云发送结果,{},{}",sendSmsResponse.getMessage(),sendSmsResponse.getCode());

        return sendSmsResponse;
    }

    /**
     * 发送报警信息
     * @param id 图片id
     * @param deviceName 设备名称
     * @param desc 描述
     * @param time 时间
     * @param phone 手机号
     * @return
     */
    public static SendSmsResponse sendSms(String id, String deviceName, String desc, String time, String phone) {

        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        try {
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        } catch (ClientException e) {
            log.error("{}",e);
        }
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(phone);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName("智能边缘");
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode("SMS_183240050");
        JSONObject param = new JSONObject();
        param.put("name", deviceName);
        param.put("type", desc);
        param.put("time", time);
        param.put("code", id);
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        // request.setTemplateParam("{\"name\":\""+deviceName+"\", \"type\":\""+desc+"\",\"code\":\""+id+"\",\"time\":"+time+"}");
        request.setTemplateParam(JSONObject.toJSONString(param));
        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = null;
        try {
            sendSmsResponse = acsClient.getAcsResponse(request);
        } catch (ServerException e) {
            log.error("{}",e);
        } catch (ClientException e) {
            log.error("{}",e);
        }
        log.info("阿里云发送结果,{},{}",sendSmsResponse.getMessage(),sendSmsResponse.getCode());
        return sendSmsResponse;
    }
}
