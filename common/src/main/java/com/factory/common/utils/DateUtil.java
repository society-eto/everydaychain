package com.factory.common.utils;

import com.alibaba.fastjson.JSON;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtil {

    /**
     * 将日期转换为字符串类型
     *
     * @param date   格式化日期
     * @param format yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getDateString(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

	/**
	 * 字符串转换成日期
	 * @param     date
	 * @return date
	 */
	public static Date stringToDate(String date, String format) throws ParseException {
		format = StringUtil.isEmpty(format) ? "yyyy-MM-dd HH:mm:ss" : format;

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);

		return simpleDateFormat.parse(date);
	}

	/**
	 * 将日期转换为字符串类型
	 *
	 * @param date 格式化日期
	 * @return
	 */
    public static String getDateString(Date date) {
        return getDateString(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 获取当前日期前后几天的日期
     *
     * @param day
     * @return
     */
    public static Date getDate(int day) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, day);
        return c.getTime();
    }

    /**
     * 获取当前日期前后几月的日期
     *
     * @param month
     * @return
     */
    public static Date getMonth(int month) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, month);
        return c.getTime();
    }

	/**
	 * 获取最近几日的日期字符串列表
	 * @param day   格式化日期
	 * @ormat yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static List<String> getDayStrings(int day,String format){
    	List<String> days = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat(format);

		Calendar c = Calendar.getInstance();

		for(; day > 0; day--){
			days.add(sdf.format(c.getTime()));
			c.add(Calendar.DATE, -1);
		}

    	return days;
	}

	/**
	 * 获取最近几日的日期字符串列表
	 * @param begin   格式化开始日期
	 * @param day   格式化日期
	 * @ormat yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static List<String> getDayStrings(Date begin, int day,String format){
		List<String> days = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat(format);

		Calendar c = Calendar.getInstance();
		c.setTime(begin);

		for(; day > 0; day--){
			days.add(sdf.format(c.getTime()));
			c.add(Calendar.DATE, -1);
		}

		return days;
	}

	/**
	 * 获取最近几小时的日期字符串列表
	 * @param hour   格式化日期
	 * @ormat yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static List<String> getHoursStrings(int hour,String format){
		List<String> hours = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat(format);

		Calendar c = Calendar.getInstance();

		for(; hour > 0; hour--){
			hours.add(sdf.format(c.getTime()));
			c.add(Calendar.HOUR, -1);
		}
		return hours;
	}

	/**
	 * 获取最近几月的月份字符串列表
	 * @param month   格式化日期
	 * @ormat yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static List<String> getMonthStrings(int month,String format){
		List<String> months = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat(format);

		Calendar c = Calendar.getInstance();

		for(; month > 0; month--){
			months.add(sdf.format(c.getTime()));
			c.add(Calendar.MONTH, -1);
		}

		return months;
	}

	/**
	 * 获取最近几小时的日期字符串列表
	 * @param day   格式化日期
	 * @ormat yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static List<String> getWeeksStrings(int day,String format){
		List<String> hours = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat(format);

		Calendar c = Calendar.getInstance();

		for(; day > 0; day--){
			hours.add(String.valueOf(c.get(Calendar.DAY_OF_WEEK) - 1));
			c.add(Calendar.DAY_OF_MONTH, -1);
		}

		return hours;
	}

	public static void main(String[] args) {
		System.out.println(JSON.toJSONString(getWeeksStrings(7,"")));
	}
}
