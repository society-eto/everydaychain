package com.factory.common.pojo;

public class Result {

    private Boolean status;

    private Object data;

    private String message;

    private Integer code;

    public Result(boolean status, Object data, String message, int code) {
        this.status = status;
        this.data = data;
        this.message = message;
        this.code = code;
    }

    public static Result success(Object data, String message) {
        return new Result(true, data, message, 200);
    }

    public static Result success(Object data) {
        return new Result(true, data, "操作成功!", 200);
    }

    public static Result success() {
        return new Result(true, null, "操作成功!", 200);
    }

    public static Result fail(Object data, String message, int code) {
        return new Result(false, data, message, code);
    }

    public static Result fail(Object data, String message) {
        return fail(data, message, 504);
    }

    public static Result fail(String message) {
        return fail(null, message);
    }

    public static Result fail() {
        return fail(null, "失败");
    }

    public Boolean isStatus() {
        return status;
    }

    public Result setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public Object getData() {
        return data;
    }

    public Result setData(Object data) {
        this.data = data;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public Result setCode(Integer code) {
        this.code = code;
        return this;
    }

}
