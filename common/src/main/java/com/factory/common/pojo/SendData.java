package com.factory.common.pojo;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 发送给终端的数据
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/2 11:22
 */
public class SendData {

    private  String id;

    private String messageId;

    private String method;

    private Object data;

    private Boolean sync = true;

    public SendData() {
    }

    public SendData(String id, String messageId, String method, Object data) {
        this.id = id;
        this.messageId = messageId;
        this.method = method;
        this.data = data;
    }

    public SendData(String id, String messageId, String method, Object data, Boolean sync) {
        this.id = id;
        this.messageId = messageId;
        this.method = method;
        this.data = data;
        this.sync = sync;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Boolean getSync() {
        return sync;
    }

    public void setSync(Boolean sync) {
        this.sync = sync;
    }

    @Override
    public String toString() {
        return "SendData{" +
                "id='" + id + '\'' +
                ", messageId='" + messageId + '\'' +
                ", method='" + method + '\'' +
                ", data=" + data +
                ", sync=" + sync +
                '}';
    }
}