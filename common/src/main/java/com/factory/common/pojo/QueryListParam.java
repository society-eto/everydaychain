package com.factory.common.pojo;

import com.factory.common.core.SqlCondition;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class QueryListParam {

    private Integer pageSize;

    private Integer pageNum;

    private List<SqlCondition> conditions;

    public Boolean contains(String key){

        if(null == conditions){
            conditions = new ArrayList<>();
            return false;
        }
        return conditions.contains(key);
    }

}