package com.factory.common.pojo;

import com.factory.common.bean.Menus;

import java.util.List;

public class MenusPojo extends Menus {

    private List<MenusPojo> children;

    public List<MenusPojo> getChildren() {
        return children;
    }

    public void setChildren(List<MenusPojo> children) {
        this.children = children;
    }
}