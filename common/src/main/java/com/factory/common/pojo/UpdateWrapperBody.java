package com.factory.common.pojo;

import com.factory.common.bean.BaseBean;
import com.factory.common.core.SqlCondition;

import java.util.List;

/**
 * 自分装传参
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/17 10:20
 */
public class UpdateWrapperBody<T extends BaseBean> {

    private T t;

    private List<SqlCondition> conditions;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public List<SqlCondition> getConditions() {
        return conditions;
    }

    public void setConditions(List<SqlCondition> conditions) {
        this.conditions = conditions;
    }
}
