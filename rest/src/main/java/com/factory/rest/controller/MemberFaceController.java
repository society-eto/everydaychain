package com.factory.rest.controller;

import com.factory.rest.feign.BaseFeignClient;
import com.factory.rest.feign.MemberFaceClient;
import com.factory.rest.pojo.MemberFacePojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/29 15:36
 */
@RestController
@RequestMapping("/rest/memberFace")
public class MemberFaceController extends BaseFeignController<MemberFacePojo> {

    @Autowired
    private MemberFaceClient memberFaceClient;


    @Override
    protected BaseFeignClient getBaseFeignClient() {
        return memberFaceClient;
    }
}
