package com.factory.rest.controller;

import com.factory.common.bean.FaceCaptured;
import com.factory.common.bean.User;
import com.factory.common.core.SqlCondition;
import com.factory.common.pojo.QueryListParam;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.BaseFeignClient;
import com.factory.rest.feign.FaceCapturedClient;
import com.factory.rest.pojo.MemberFacePojo;
import com.factory.rest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 人脸抓拍的控制器
 *
 * @author JW
 * @version 1.0
 * @date 2021/1/14 15:55
 */
@RestController
@RequestMapping("/rest/faceCaptured")
public class FaceCapturedController extends BaseFeignController<FaceCaptured>{

    @Autowired
    private FaceCapturedClient faceCapturedClient;

    @Override
    protected BaseFeignClient getBaseFeignClient() {
        return faceCapturedClient;
    }

}
