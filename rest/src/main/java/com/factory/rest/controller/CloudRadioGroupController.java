package com.factory.rest.controller;

import com.factory.common.bean.CloudRadioGroup;
import com.factory.common.bean.User;
import com.factory.common.core.BaseService;
import com.factory.common.core.ValidateType;
import com.factory.common.pojo.Result;
import com.factory.common.utils.DateUtil;
import com.factory.common.utils.FileUtils;
import com.factory.common.utils.StringUtil;
import com.factory.rest.pojo.CloudRadioGroupPojo;
import com.factory.rest.pojo.CloudRadioPojo;
import com.factory.rest.service.CloudRadioGroupService;
import com.factory.rest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.Date;
import java.util.Optional;

/**
 * 云广播分组配置器
 *
 * @author JW
 * @version 1.0
 * @date 2021/5/18 15:57
 */
@RestController
@RequestMapping("/rest/cloudRadioGroup")
public class CloudRadioGroupController extends BaseController<CloudRadioGroupPojo>{

    @Autowired
    private CloudRadioGroupService cloudRadioGroupService;

    @Override
    protected BaseService getBaseService() {
        return cloudRadioGroupService;
    }
}
