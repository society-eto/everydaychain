package com.factory.rest.controller;

import com.factory.common.bean.Config;
import com.factory.common.core.BaseService;
import com.factory.rest.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统配置控制器
 *
 * @author JW
 * @version 1.0
 * @date 2021/5/18 15:53
 */
@RestController
@RequestMapping("/rest/config")
public class ConfigController extends BaseController<Config>{

    @Autowired
    private ConfigService configService;

    @Override
    protected BaseService getBaseService() {
        return configService;
    }
}
