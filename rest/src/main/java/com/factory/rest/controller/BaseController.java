package com.factory.rest.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.factory.common.bean.BaseBean;
import com.factory.common.bean.User;
import com.factory.common.core.BaseService;
import com.factory.common.core.SqlCondition;
import com.factory.common.core.ValidateType;
import com.factory.common.pojo.QueryListParam;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;
import com.factory.rest.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Optional;

/**
 *
 * 本地的调用控制器通用类 controller
 * @param <T> 实体类对象
 *
 */
public abstract class BaseController<T extends BaseBean> {

    protected static final Logger log = LoggerFactory.getLogger(BaseController.class);

    protected abstract BaseService getBaseService();

    @Autowired
    protected UserService userService;

    /**
     * T
     * 通用的添加方法
     *
     * @return 用户列表
     */

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Result insert(@RequestBody T t) {
        log.info("通用的添加数据方法,{}", t);
        if (t == null) {
            return Result.fail(500, "request body can not null!");
        }
        Optional<User> loginUser = userService.getLoginUser();
        if (loginUser.isPresent()){
            User user = loginUser.get();
            t.setUser(user);
        }

        //校验
        Optional<Result> validate = t.validate(ValidateType.INSERT);
        if (validate.isPresent()) {
            return validate.get();
        }


        Integer result = getBaseService().insert(t);
        if (1 == result) {
            return Result.success("", null);
        }

        return Result.fail(500, "insert fail!");
    }

    /**
     * 通用的查一条数据方法
     *
     * @return 一条数据
     */
    @RequestMapping(value = "/query/{id}", method = RequestMethod.GET)
    public Result query(@PathVariable String id) {
        log.info("通用的查一条数据方法,{}", id);
        if (id == null) {
            return Result.fail(500, "request id can not null!");
        }
        return Result.success(getBaseService().selectById(id), "操作成功");
    }

    /**
     * 通用的删除一条数据方法
     *
     * @return 删除一条数据
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public Result delete(@PathVariable String id) {
        log.info("通用的查一条数据方法,{}", id);
        if (id == null) {
            return Result.fail(500, "request id can not null!");
        }
        Integer result = getBaseService().deleteById(id);
        if (1 == result) {
            return Result.success("成功", null);
        }
        return Result.fail(500, "删除失败!");
    }

    /**
     * 通用的修改一条数据方法
     *
     * @return 修改一条数据
     */

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result update(@RequestBody T t) {
        log.info("通用的修改一条数据方法,{}", t);
        if (t == null) {
            return Result.fail(500, "request update body can not null!");
        }
        //校验
        Optional<Result> validate = t.validate(ValidateType.UPDATE);
        if (validate.isPresent()) {
            return validate.get();
        }

        Integer result = getBaseService().updateById(t);
        if (1 == result) {
            return Result.success(null, "操作成功");
        }
        return Result.fail(504, "失败!");
    }

    /**
     * 通用查询数据列表的方法，含有分页的功能
     * 子类继承父类即可使用该方法
     * {
     * "pageSize":10,
     * "pageNum":1,
     * "conditions":[{"name":"account","value":"123456","method":"eq"}]
     * }
     *
     * @return 查询到的数据列表集合
     */

    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    public Result queryList(@RequestBody QueryListParam param) {
        log.info("通用查询数据列表的方法,{}", param);

        if (param == null) {
            return Result.fail(500, "request param body can not null!");
        }

        User user = userService.getLoginUser().get();

        if(null != user){
            List<SqlCondition> conditions = param.getConditions();
            conditions.add(new SqlCondition("userId", user.getId(), SqlCondition.Conditions.EQ));
        }

        Page<T> pageEntity = new Page<>();
        pageEntity.setSize(StringUtil.toInteger(param.getPageSize(), 10));
        pageEntity.setCurrent(StringUtil.toInteger(param.getPageNum(), 1));

        QueryWrapper<T> wrapper = new QueryWrapper<>();
        SqlCondition order = null;
        for(SqlCondition e : param.getConditions()){
            if(StringUtil.notEmpty(e.getValue())){
                switch (e.getMethod()) {
                    case SqlCondition.Conditions.EQ:
                        wrapper.eq(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.LIKE:
                        wrapper.like(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.GE:
                        wrapper.ge(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.LE:
                        wrapper.le(StringUtil.underscoreName(e.getKey()), e.getValue());
                        break;
                    case SqlCondition.Conditions.DESC:
                        order = e;
                        break;
                    case SqlCondition.Conditions.ASC:
                        order = e;
                        break;
                }
            }
        }

        if(null != order){
            if(SqlCondition.Conditions.DESC.equals(order.getMethod())){
                wrapper.orderByDesc(StringUtil.underscoreName(order.getKey()));
            }else {
                wrapper.orderByAsc(StringUtil.underscoreName(order.getKey()));
            }
        }

        return Result.success(getBaseService().selectPage(pageEntity, wrapper), "操作成功");
    }
}