package com.factory.rest.controller;

import com.factory.common.bean.User;
import com.factory.common.core.BaseService;
import com.factory.common.core.SqlCondition;
import com.factory.common.pojo.QueryListParam;
import com.factory.common.pojo.Result;
import com.factory.rest.pojo.LocationPojo;
import com.factory.rest.service.LocationService;
import com.factory.rest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * 位置信息控制器
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/2 15:07
 */
@RestController
@RequestMapping("/rest/location")
public class LocationController extends BaseController<LocationPojo>{

    @Autowired
    private LocationService locationService;

    @Autowired
    private UserService userService;

    @Override
    protected BaseService getBaseService() {
        return this.locationService;
    }

    @Override
    public Result queryList(@RequestBody QueryListParam param) {
        User user = userService.getLoginUser().get();

        List<SqlCondition> conditions = param.getConditions();
        conditions.add(new SqlCondition("userId",user.getId(),SqlCondition.Conditions.EQ));

        return super.queryList(param);
    }

    @Override
    public Result insert(@RequestBody LocationPojo locationPojo) {
        log.info("添加位置信息,{}",locationPojo);
        User user = userService.getLoginUser().get();
        locationPojo.setUserId(user.getId());
        locationPojo.setUserName(user.getName());
        locationPojo.setCreateDate(new Date());

        return super.insert(locationPojo);
    }
}
