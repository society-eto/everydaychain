package com.factory.rest.controller;

import com.factory.common.bean.Algorithm;
import com.factory.common.bean.Devices;
import com.factory.common.core.BaseService;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;
import com.factory.rest.service.AlgorithmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * 算法/功能控制器
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/2 17:36
 */
@RestController
@RequestMapping("/rest/algorithm")
@Slf4j
public class AlgorithmController  extends BaseController<Devices>{

    @Autowired
    private AlgorithmService algorithmService;

    /**
     * 查询设备含有的算法列表
     *
     * @return
     */
    @PostMapping("/devicesAlgorithmList")
    public Result devicesAlgorithmList(@RequestBody Map<String, Object> params) {
        log.info("查询设备含有的算法列表,{}", params);
        if (StringUtil.isEmpty(params.get("userDevicesId"))) {
            return Result.fail("userDevicesId不能为空!");
        }
        return Result.success(algorithmService.devicesAlgorithmList(params));
    }

    @Override
    protected BaseService getBaseService() {
        return algorithmService;
    }
}
