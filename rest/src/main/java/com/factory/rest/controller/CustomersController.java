package com.factory.rest.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.factory.common.bean.Location;
import com.factory.common.bean.User;
import com.factory.common.pojo.Result;
import com.factory.common.utils.DateUtil;
import com.factory.common.utils.StringUtil;
import com.factory.rest.constant.Regex;
import com.factory.rest.feign.CustomersClient;
import com.factory.rest.service.LocationService;
import com.factory.rest.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 客流接口
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/11 11:38
 */
@RequestMapping("/rest/customers")
@RestController
public class CustomersController {

    private static final Logger log = LoggerFactory.getLogger(CustomersController.class);

    @Autowired
    private CustomersClient customersClient;

    @Autowired
    private UserService userService;

    @Autowired
    private LocationService locationService;

    /**
     * 客流数据统计
     *
     * @return
     */
    @PostMapping("/statistics")
    public Result statistics(@RequestBody Map<String, Object> param) {
        log.info("客流数据统计,{}", param);

        if (StringUtil.val(param.get("date"), "").equals("day")) {
            param.put("beginDate", DateUtil.getDateString(DateUtil.getDate(-7)));
        }

        return Result.success(null);
    }

    /**
     * 客流数据日统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsDay")
    public Result statisticsDay(@RequestBody Map<String, Object> params) {
        log.info("客流数据日统计,{}", params);
        User user = userService.getLoginUser().get();

        params.put("userId", user.getId());
        params.put("beginDate", DateUtil.getDateString(DateUtil.getDate(-1)));

        Map<String, Object> result = new HashMap<>();
        Result result1 = customersClient.statisticsDay(params);
        if (result1.isStatus()) {
            result.put("statistics", result1.getData());
            List<String> hoursStrings = DateUtil.getHoursStrings(24, "HH:00");
            Collections.reverse(hoursStrings);
            result.put("date", hoursStrings);
            return Result.success(result);
        }
        return Result.fail("失败!");
    }

    /**
     * 客流数据周统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsWeek")
    public Result statisticsWeek(@RequestBody Map<String, Object> params) {
        log.info("客流数据周统计,{}", params);

        User user = userService.getLoginUser().get();

        params.put("userId", user.getId());
        params.put("beginDate", DateUtil.getDateString(DateUtil.getDate(-7)));

        Result result1 = customersClient.statisticsWeek(params);
        if (result1.isStatus()) {
            Map<String, Object> result = new HashMap<>();
            result.put("statistics", result1.getData());
            List<String> weeksStrings = DateUtil.getWeeksStrings(7, "");
            Collections.reverse(weeksStrings);
            result.put("date", weeksStrings);
            return Result.success(result);
        }
        return Result.fail("失败!");
    }


    /**
     * 客流数据月统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsMonth")
    public Result statisticsMonth(@RequestBody Map<String, Object> params) {
        log.info("客流数据月统计,{}", params);
        User user = userService.getLoginUser().get();
        params.put("userId", user.getId());

        if(StringUtil.isEmpty(params.get("beginDate"))){
            params.put("beginDate", DateUtil.getDateString(DateUtil.getDate(-30)));
        }else if(!StringUtil.matches(params.get("beginDate"), Regex.dateTime)) {
            return Result.fail("beginDate 格式不正确!");
        }

        if(StringUtil.notEmpty("endDate") && !StringUtil.matches(params.get("beginDate"),Regex.dateTime)){
            return Result.fail("endDate 格式不正确!");
        }

        long day = 30;
        Date date = new Date();
        if(StringUtil.notEmpty(params.get("endDate"))){
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime endDate = LocalDateTime.parse(params.get("endDate").toString(), dateTimeFormatter);
            date = Date.from( endDate.atZone(ZoneId.systemDefault()).toInstant());
            day = Duration.between(LocalDateTime.parse(params.get("beginDate").toString(), dateTimeFormatter),
                    endDate).toDays();
        }

        Result result1 = customersClient.statisticsMonth(params);
        if (result1.isStatus()) {
            Map<String, Object> result = new HashMap<>();
            result.put("statistics", result1.getData());

            List<String> dayStrings = DateUtil.getDayStrings(date, (int) day, "MM-dd");
            Collections.reverse(dayStrings);
            result.put("date", dayStrings);

            return Result.success(result);
        }
        return Result.fail("失败!");
    }

    /**
     * 客流数据季度统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsQuarter")
    public Result statisticsQuarter(@RequestBody Map<String, Object> params) {
        log.info("客流数据季度统计,{}", params);

        User user = userService.getLoginUser().get();

        params.put("userId", user.getId());
//      params.put("beginDate", DateUtil.getDateString(DateUtil.getMonth(-2)));

        Result result1 = customersClient.statisticsQuarter(params);
        if (result1.isStatus()) {
            Map<String, Object> result = new HashMap<>();
            result.put("statistics", result1.getData());
            return Result.success(result);
        }

        return Result.fail("失败!");
    }

    /**
     * 获取地址列表
     *
     * @param params
     * @return
     */
    @PostMapping("/queryLocationList")
    public Result queryLocationList(@RequestBody Map<String, Object> params) {
        log.info("客流数据月统计,{}", params);
        User user = userService.getLoginUser().get();
        return Result.success(locationService.selectList(new QueryWrapper<Location>().ge("status", 0).eq("user_id", user.getId())));
    }

    /**
     * 性别数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsGender")
    public Result statisticsGender(@RequestBody Map<String, Object> params) {
        log.info("性别数据统计,{}", params);
        User user = userService.getLoginUser().get();
        params.put("userId", user.getId());
        return customersClient.statisticsGender(params);
    }

    /**
     * 年龄数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsAge")
    public Result statisticsAge(@RequestBody Map<String, Object> params) {
        log.info("性别数据统计,{}", params);
        User user = userService.getLoginUser().get();
        params.put("userId", user.getId());
        return customersClient.statisticsAge(params);
    }

    /**
     * 年龄数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsAgeCustomers")
    public Result statisticsAgeCustomers(@RequestBody Map<String, Object> params) {
        log.info("性别数据统计,{}", params);
        User user = userService.getLoginUser().get();
        params.put("userId", user.getId());
        return customersClient.statisticsAgeCustomers(params);
    }

    /**
     * 店铺数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsLocation")
    public Result statisticsLocation(@RequestBody Map<String, Object> params) {
        log.info("店铺数据统计,{}", params);
        User user = userService.getLoginUser().get();
        params.put("userId", user.getId());
        return customersClient.statisticsLocation(params);
    }

    /**
     * 店铺数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsCustomers")
    public Result statisticsCustomers(@RequestBody Map<String, Object> params) {
        log.info("店铺数据统计,{}", params);
        User user = userService.getLoginUser().get();
        params.put("userId", user.getId());
        return customersClient.statisticsCustomers(params);
    }
}