package com.factory.rest.controller;

import com.factory.rest.feign.BaseFeignClient;
import com.factory.rest.feign.MemberClient;
import com.factory.rest.pojo.MemberPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户控制器
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/28 9:49
 */
@RequestMapping("/rest/member")
@RestController
public class MemberController extends BaseFeignController<MemberPojo>{

    @Autowired
    private MemberClient memberClient;

    @Override
    protected BaseFeignClient getBaseFeignClient() {
        return this.memberClient;
    }
}
