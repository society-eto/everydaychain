package com.factory.rest.controller;

import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;
import com.factory.rest.constant.Regex;
import com.factory.rest.feign.BaseFeignClient;
import com.factory.rest.feign.TurnoverClient;
import com.factory.rest.pojo.TurnoverPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

/**
 * 用户控制器
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/28 9:49
 */
@RequestMapping("/rest/turnover")
@RestController
public class TurnoverController extends BaseFeignController<TurnoverPojo> {

    @Autowired
    private TurnoverClient turnoverClient;

    @Override
    protected BaseFeignClient getBaseFeignClient() {
        return this.turnoverClient;
    }

    /**
     * 营业数据统计接口
     *
     * @param params
     * @return
     */
    @PostMapping("/statistics")
    public Result statistics(@RequestBody Map<String, Object> params) {
        log.info("营业额数据统计接口,{}", params);
        params.put("userId", getUser().getId());
        if (StringUtil.notEmpty(params.get("pageSize")) && StringUtil.notEmpty(params.get("pageNum"))) {
            params.put("start", StringUtil.toInteger(params.get("pageSize"), 10) * (StringUtil.toInteger(params.get("pageNum"), 1) - 1));
            params.put("limit", params.get("pageSize"));
        }

        if(!StringUtil.matches(params.get("beginDate"), Regex.dateTime) && !StringUtil.matches(params.get("beginDate"), Regex.jsonDateTime)){
            return Result.fail("beginDate格式不正确!");
        }

        if(!StringUtil.matches(params.get("endDate"), Regex.dateTime) && !StringUtil.matches(params.get("endDate"), Regex.jsonDateTime)){
            return Result.fail("endDate格式不正确!");
        }

        return Result.success(turnoverClient.statistics(params));
    }

}
