package com.factory.rest.controller;

import com.factory.common.bean.Devices;
import com.factory.common.core.BaseService;
import com.factory.rest.service.DevicesService;
import com.factory.rest.service.UserService;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.mitre.dsmiley.httpproxy.ProxyServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 设备管理器
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/28 10:38
 */
@RestController
@RequestMapping("/rest/devices")
public class DevicesController extends BaseController<Devices> {

    @Autowired
    private DevicesService devicesService;

    @Autowired
    private UserService userService;

    @Override
    protected BaseService getBaseService() {
        return this.devicesService;
    }


    public void proxyServlet(){


    }

    class DeviceProxyServlet extends ProxyServlet{

        String url;

        public DeviceProxyServlet(String url){
            this.url = url;
        }

        @Override
        public HttpResponse doExecute(HttpServletRequest servletRequest, HttpServletResponse servletResponse, HttpRequest proxyRequest) throws IOException {
            return super.doExecute(servletRequest, servletResponse, newProxyRequestWithEntity(servletRequest.getMethod(), this.url, servletRequest));
        }
    }
}
