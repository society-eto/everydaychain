package com.factory.rest.controller;

import com.factory.common.pojo.Result;
import com.factory.common.utils.DateUtil;
import com.factory.common.utils.FileUtils;
import com.factory.common.utils.ImgUtil;
import com.factory.common.utils.StringUtil;
import com.factory.rest.feign.BaseFeignClient;
import com.factory.rest.feign.CameraWheelLogClient;
import com.factory.rest.pojo.CameraWheelLogPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.Date;
import java.util.Optional;

/**
 * 用户控制器
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/28 9:49
 */
@RequestMapping("/rest/cameraWheelLog")
@RestController
public class CameraWheelLogController extends BaseFeignController<CameraWheelLogPojo>{

    @Autowired
    private CameraWheelLogClient cameraWheelLogClient;

    @Value("${jiexun.upload.dir}")
    private String uploadDir;

    @Override
    protected BaseFeignClient getBaseFeignClient() {
        return this.cameraWheelLogClient;
    }


    @Override
    public Result insert(@RequestBody CameraWheelLogPojo cameraWheelLogPojo) {

        cameraWheelLogPojo.setUser(getUser());
        Optional<Result> result = cameraWheelLogPojo.vInsert();
        if(result.isPresent()){
            return result.get();
        }

        String image = cameraWheelLogPojo.getBase64Image();

        if(StringUtil.notEmpty(image)){
            String filePath =  "wheel" + File.separator + DateUtil.getDateString(new Date(), "yyyy-MM-dd"), fileName = cameraWheelLogPojo.getId() + ".jpg";
            ImgUtil.generateBase64Image(image, uploadDir + filePath, fileName);
            cameraWheelLogPojo.setImage(filePath + File.separator + fileName);
        }

        return cameraWheelLogClient.insert(cameraWheelLogPojo);
    }
}
