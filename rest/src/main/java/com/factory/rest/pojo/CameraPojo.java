package com.factory.rest.pojo;

import com.factory.common.bean.Camera;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 摄像头包装类
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/17 15:08
 */
public class CameraPojo extends Camera {

    @Override
    public Optional<Result> vInsert() {
        if(StringUtil.isEmpty(getAccount())){
            return Optional.of(Result.fail("account 不能为空!"));
        }else if(StringUtil.isEmpty(getPassword())){
            return Optional.of(Result.fail("password 不能为空!"));
        }else if(StringUtil.isEmpty(getIp())){
            return Optional.of(Result.fail("ip 不能为空!"));
        }else if(StringUtil.isEmpty(getPort())){
            return Optional.of(Result.fail("port 不能为空!"));
        }else if(StringUtil.isEmpty(getName())){
            return Optional.of(Result.fail("name 不能为空!"));
        }else if(StringUtil.isEmpty(getDeviceId())){
            return Optional.of(Result.fail("deviceId 不能为空!"));
        }

        return Optional.empty();
    }

    @Override
    public Optional<Result> vUpdate() {
        if(StringUtil.isEmpty(getAccount())){
            return Optional.of(Result.fail("account 不能为空!"));
        }else if(StringUtil.isEmpty(getPassword())){
            return Optional.of(Result.fail("password 不能为空!"));
        }else if(StringUtil.isEmpty(getIp())){
            return Optional.of(Result.fail("ip 不能为空!"));
        }else if(StringUtil.isEmpty(getPort())){
            return Optional.of(Result.fail("port 不能为空!"));
        }else if(StringUtil.isEmpty(getName())){
            return Optional.of(Result.fail("name 不能为空!"));
        }

        return Optional.empty();
    }

    private List<String> functionsItem;

    public List<String> getFunctionsItem() {
        if(StringUtil.notEmpty(getFunctions())){
            return Arrays.asList(getFunctions().split(","));
        }
        return functionsItem;
    }

    public void setFunctionsItem(List<String> functionsItem) {
        if(StringUtil.notEmpty(functionsItem)){
            setFunctions(String.join(",",functionsItem));
        }
        this.functionsItem = functionsItem;
    }

    private Boolean isOnline;

    public Boolean getOnline() {
        return isOnline;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }
}
