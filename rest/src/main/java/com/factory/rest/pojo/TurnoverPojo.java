package com.factory.rest.pojo;

import com.factory.common.bean.CameraWheelLog;
import com.factory.common.bean.CustomersDoorway;
import com.factory.common.bean.Turnover;
import com.factory.common.bean.User;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;
import com.factory.rest.constant.Regex;

import java.util.Date;
import java.util.Optional;

/**
 * 摄像头包装类
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/17 15:08
 */
public class TurnoverPojo extends Turnover {

    @Override
    public Optional<Result> vInsert() {

        if(StringUtil.isEmpty(getOrders())){
            return Optional.of(Result.fail("orders不能为空!"));
        }else if(StringUtil.isEmpty(getOrdersDate())){
            return Optional.of(Result.fail("ordersDate不能为空!"));
        }else if(StringUtil.isEmpty(getSale())){
            return Optional.of(Result.fail("sale不能为空!"));
        }else if(StringUtil.isEmpty(getLocationId())){
            return Optional.of(Result.fail("location id 不能为空!"));
        }else if(StringUtil.isEmpty(getLocationName())){
            return Optional.of(Result.fail("location name 不能为空!"));
        }

        setId(StringUtil.getUUID());
        setCreateDate(new Date());

        User user = getUser();
        setUserId(user.getId());
        setUserName(user.getName());

        return Optional.empty();
    }

}
