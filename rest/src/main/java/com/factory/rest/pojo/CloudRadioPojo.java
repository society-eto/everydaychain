package com.factory.rest.pojo;

import com.factory.common.bean.CloudRadio;
import com.factory.common.pojo.Result;
import com.factory.common.utils.DateUtil;
import com.factory.common.utils.FileUtils;
import com.factory.common.utils.StringUtil;

import java.io.File;
import java.util.Date;
import java.util.Optional;

/**
 * 云广播包装类
 *
 * @author JW
 * @version 1.0
 * @date 2021/5/18 16:09
 */
public class CloudRadioPojo extends CloudRadio {

    @Override
    protected Optional<Result> vInsert() {

        if(StringUtil.isEmpty(getName()))
            return Optional.of(Result.fail("name不能为空"));
        else if(StringUtil.isEmpty(getPath()))
            return Optional.of(Result.fail("path不能为空"));

        setId(StringUtil.getUUID());
        setCreateDate(new Date());

        return super.vInsert();
    }

    @Override
    protected Optional<Result> vDelete() {
        return super.vDelete();
    }

    @Override
    protected Optional<Result> vUpdate() {

        if(StringUtil.isEmpty(getName()))
            return Optional.of(Result.fail("name不能为空"));
        else if(StringUtil.isEmpty(getPath()))
            return Optional.of(Result.fail("path不能为空"));
        else if(StringUtil.isEmpty(getId()))
            return Optional.of(Result.fail("id不能为空"));

        return super.vUpdate();
    }

    @Override
    protected Optional<Result> vQuery() {
        return super.vQuery();
    }

    @Override
    protected Optional<Result> vQueryList() {
        return super.vQueryList();
    }
}
