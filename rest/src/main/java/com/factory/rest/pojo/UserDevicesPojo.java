package com.factory.rest.pojo;

import com.factory.common.bean.UserDevices;
import com.factory.rest.controller.BaseController;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/28 10:49
 */
public class UserDevicesPojo extends UserDevices {

    private Float cpu;

    private  Float gpu;

    private  Float memory;

    public Float getCpu() {
        return cpu;
    }

    public void setCpu(Float cpu) {
        this.cpu = cpu;
    }

    public Float getGpu() {
        return gpu;
    }

    public void setGpu(Float gpu) {
        this.gpu = gpu;
    }

    public Float getMemory() {
        return memory;
    }

    public void setMemory(Float memory) {
        this.memory = memory;
    }
}
