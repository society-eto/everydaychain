package com.factory.rest.pojo;

import com.factory.common.bean.CameraWheelLog;
import com.factory.common.bean.User;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;

import java.util.Date;
import java.util.Optional;

/**
 * 摄像头包装类
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/17 15:08
 */
public class CameraWheelLogPojo extends CameraWheelLog {

    @Override
    public Optional<Result> vInsert() {

        if(StringUtil.isEmpty(getCameraId())){
            return Optional.of(Result.fail("camera id 不能为空!"));
        }else if(StringUtil.isEmpty(getCameraName())){
            return Optional.of(Result.fail("camera name 不能为空!"));
        }else if(StringUtil.isEmpty(getBase64Image())){
            return Optional.of(Result.fail("base64Image 不能为空!"));
        }else if(StringUtil.isEmpty(getLocationId())){
            return Optional.of(Result.fail("location id 不能为空!"));
        }else if(StringUtil.isEmpty(getLocationName())){
            return Optional.of(Result.fail("location name 不能为空!"));
        }else if(StringUtil.isEmpty(getNum())){
            return Optional.of(Result.fail("num不能为空!"));
        }else if(StringUtil.isEmpty(getName())){
            return Optional.of(Result.fail("name 不能为空!"));
        }else if(StringUtil.isEmpty(getResult())){
            return Optional.of(Result.fail("result 不能为空!"));
        }

        setId(StringUtil.getUUID());
        setCreateDate(new Date());

        User user = getUser();
        setUserId(user.getId());
        setUserName(user.getName());
        setCreateName(user.getName());

        return Optional.empty();
    }

    @Override
    public Optional<Result> vUpdate() {
        return Optional.empty();
    }

    private String base64Image;

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }
}
