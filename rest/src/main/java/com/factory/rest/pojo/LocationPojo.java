package com.factory.rest.pojo;

import com.factory.common.bean.Location;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;

import java.util.Optional;

/**
 * 位置信息
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/2 15:08
 */
public class LocationPojo extends Location {

    @Override
    public Optional<Result> vInsert() {
        if(StringUtil.isEmpty(getName())){
            return Optional.of(Result.fail("name 不能为空!"));
        }else if(StringUtil.isEmpty(getAddress())){
            return Optional.of(Result.fail("address 不能为空!"));
        }

        return Optional.empty();
    }

    @Override
    public Optional<Result> vUpdate() {
        if(StringUtil.isEmpty(getId())){
            return Optional.of(Result.fail("id 不能为空!"));
        }else if(StringUtil.isEmpty(getName())){
            return Optional.of(Result.fail("name 不能为空!"));
        }else if(StringUtil.isEmpty(getAddress())){
            return Optional.of(Result.fail("address 不能为空!"));
        }

        return Optional.empty();
    }
}
