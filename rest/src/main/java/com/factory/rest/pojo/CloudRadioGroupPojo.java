package com.factory.rest.pojo;

import com.factory.common.bean.CloudRadioGroup;
import com.factory.common.bean.User;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;

import java.util.Optional;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2021/5/19 10:45
 */
public class CloudRadioGroupPojo extends CloudRadioGroup {

    @Override
    protected Optional<Result> vInsert() {
        if(StringUtil.isEmpty(getName())){
            return Optional.of(Result.fail("name不能为空!"));
        }
        setId(StringUtil.getUUID());

        User user = getUser();
        if(null != user){
            setUserId(user.getId());
            setUserName(user.getName());
        }

        return super.vInsert();
    }

    @Override
    protected Optional<Result> vUpdate() {
        if(StringUtil.isEmpty(getName())){
            return Optional.of(Result.fail("name不能为空!"));
        }else if(StringUtil.isEmpty(getId())){
            return Optional.of(Result.fail("id不能为空!"));
        }
        return super.vUpdate();
    }
}
