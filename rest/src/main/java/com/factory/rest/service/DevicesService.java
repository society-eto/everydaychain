package com.factory.rest.service;

import com.factory.common.bean.Devices;
import com.factory.common.core.BaseService;

import java.util.Map;

public interface DevicesService extends BaseService<Devices> {
}