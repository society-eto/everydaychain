package com.factory.rest.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Location;
import com.factory.common.core.BaseServiceImpl;
import com.factory.common.mapper.LocationMapper;
import com.factory.rest.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/12 9:33
 */
@Service
public class LocationServiceImpl extends BaseServiceImpl<Location> implements LocationService {

    @Autowired
    private LocationMapper locationMapper;

    @Override
    protected BaseMapper<Location> getBaseMapper() {
        return this.locationMapper;
    }
}
