package com.factory.rest.service;

import com.alibaba.fastjson.JSONObject;
import com.factory.common.bean.User;
import com.factory.common.bean.UserWx;
import com.factory.common.core.BaseService;
import com.factory.common.pojo.MenusPojo;
import com.factory.common.pojo.Result;

import java.util.List;
import java.util.Optional;

public interface UserService extends BaseService<User> {

    /**
     * 用户登录
     * @param name
     * @param pass
     * @return
     */
    Result login(String name, String pass);

    /**
     * 添加用户session
     * @param user
     * @return
     */
    public JSONObject setSession(User user);

    /**
     * 用户退出登录
     * @return
     */
    Result loginOut();

    /**
     * 获取当前登录用户
     * @return
     */
    Optional<User> getLoginUser();

    /**
     * 查询当前用户的菜单
     * @return
     * @param userId
     */
    List<MenusPojo> queryUserMenus(String userId);

    /**
     * 根据用户openID查询用户信息
     * @param openId
     * @return
     */
    User queryByOpenId(String openId);

    /**
     * 添加微信绑定
     * @param userWx
     * @return
     */
    Integer insertWx(UserWx userWx);

}
