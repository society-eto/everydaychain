package com.factory.rest.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Devices;
import com.factory.common.core.BaseServiceImpl;
import com.factory.common.mapper.DevicesMapper;
import com.factory.rest.service.DevicesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DevicesServiceImpl extends BaseServiceImpl<Devices> implements DevicesService {

    @Autowired
    private DevicesMapper devicesMapper;

    @Override
    protected BaseMapper<Devices> getBaseMapper() {
        return this.devicesMapper;
    }
}