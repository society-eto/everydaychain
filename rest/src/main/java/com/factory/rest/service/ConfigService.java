package com.factory.rest.service;

import com.factory.common.bean.Config;
import com.factory.common.core.BaseService;

/**
 * 系统服务配置类
 *
 * @author JW
 * @version 1.0
 * @date 2021/5/18 15:39
 */
public interface ConfigService extends BaseService<Config> {
}
