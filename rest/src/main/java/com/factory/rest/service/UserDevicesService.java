package com.factory.rest.service;

import com.factory.common.bean.UserDevices;
import com.factory.common.core.BaseService;

import java.util.Map;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/27 11:32
 */
public interface UserDevicesService extends BaseService<UserDevices> {

    /**
     * 设备数据统计
     * @param params
     * @return
     */
    Map<String,Object> statistics(Map<String,Object> params);
}