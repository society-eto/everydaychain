package com.factory.rest.service;

import com.factory.common.bean.CloudRadioGroup;
import com.factory.common.core.BaseService;

/**
 * 云广播
 *
 * @author JW
 * @version 1.0
 * @date 2021/5/18 15:38
 */
public interface CloudRadioGroupService extends BaseService<CloudRadioGroup> {
}
