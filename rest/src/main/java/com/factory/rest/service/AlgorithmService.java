package com.factory.rest.service;

import com.factory.common.bean.Algorithm;
import com.factory.common.core.BaseService;

import java.util.List;
import java.util.Map;

/**
 * 算法服务类
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/2 17:38
 */
public interface AlgorithmService extends BaseService<Algorithm> {

    /**
     * 查询设备含有的算法列表
     * @param params
     * @return
     */
    List<Algorithm> devicesAlgorithmList(Map<String,Object> params);
}
