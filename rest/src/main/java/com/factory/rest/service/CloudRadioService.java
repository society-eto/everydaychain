package com.factory.rest.service;

import com.factory.common.bean.CloudRadio;
import com.factory.common.bean.Config;
import com.factory.common.core.BaseService;

/**
 * 云广播业务类
 *
 * @author JW
 * @version 1.0
 * @date 2021/5/18 14:15
 */
public interface CloudRadioService extends BaseService<CloudRadio> {

}
