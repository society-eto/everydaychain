package com.factory.rest.service;

/**
 * 异步处理服务类
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/3 10:11
 */
public interface AsyncService {

    /**
     * 发送验证码
     * @param code 验证码
     * @param mobile 手机号
     * @return
     */
    void sendCode(String mobile,String code);
}
