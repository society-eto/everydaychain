package com.factory.rest.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.CloudRadioGroup;
import com.factory.common.core.BaseServiceImpl;
import com.factory.common.mapper.CloudRadioGroupMapper;
import com.factory.common.mapper.CloudRadioMapper;
import com.factory.rest.service.CloudRadioGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 云广播分组
 *
 * @author JW
 * @version 1.0
 * @date 2021/5/18 15:44
 */
@Service
public class CloudRadioCroupServiceImpl extends BaseServiceImpl<CloudRadioGroup> implements CloudRadioGroupService {

    @Autowired
    private CloudRadioGroupMapper cloudRadioGroupMapper;

    @Override
    protected BaseMapper<CloudRadioGroup> getBaseMapper() {
        return cloudRadioGroupMapper;
    }
}
