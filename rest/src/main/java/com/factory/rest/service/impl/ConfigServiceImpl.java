package com.factory.rest.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Config;
import com.factory.common.core.BaseServiceImpl;
import com.factory.common.mapper.ConfigMapper;
import com.factory.rest.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2021/5/18 15:45
 */
@Service
public class ConfigServiceImpl extends BaseServiceImpl<Config> implements ConfigService {

    @Autowired
    private ConfigMapper configMapper;

    @Override
    protected BaseMapper<Config> getBaseMapper() {
        return configMapper;
    }
}
