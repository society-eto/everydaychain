package com.factory.rest.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.UserDevices;
import com.factory.common.core.BaseServiceImpl;
import com.factory.common.mapper.UserDevicesMapper;
import com.factory.rest.service.UserDevicesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/27 11:32
 */
@Service
@Slf4j
public class UserDeviceServiceImpl extends BaseServiceImpl<UserDevices> implements UserDevicesService {

    @Autowired
    private UserDevicesMapper userDevicesMapper;

    @Override
    protected BaseMapper<UserDevices> getBaseMapper() {
        return this.userDevicesMapper;
    }

    @Override
    public Map<String, Object> statistics(Map<String, Object> params) {
        return userDevicesMapper.statistics(params);
    }
}