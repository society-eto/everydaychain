package com.factory.rest.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.common.bean.Algorithm;
import com.factory.common.core.BaseServiceImpl;
import com.factory.common.mapper.AlgorithmMapper;
import com.factory.rest.service.AlgorithmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 算法服务类
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/2 17:38
 */
@Service
public class AlgorithmImpl extends BaseServiceImpl<Algorithm> implements AlgorithmService {

    @Autowired
    private AlgorithmMapper algorithmMapper;

    @Override
    public List<Algorithm> devicesAlgorithmList(Map<String, Object> params) {
        return algorithmMapper.devicesAlgorithmList(params);
    }

    @Override
    protected BaseMapper<Algorithm> getBaseMapper() {
        return algorithmMapper;
    }
}
