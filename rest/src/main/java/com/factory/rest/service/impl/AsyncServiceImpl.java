package com.factory.rest.service.impl;

import com.factory.common.utils.SmsUtils;
import com.factory.rest.service.AsyncService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/3 10:13
 */
@Service
@Slf4j
public class AsyncServiceImpl implements AsyncService {

    @Async
    @Override
    public void sendCode(String mobile, String code) {
        log.info("发送验证码,{},{}",mobile,code);
        SmsUtils.sendCode(code, mobile);
    }
}
