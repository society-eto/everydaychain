package com.factory.rest.feign.fallback;

import com.factory.common.bean.Speech;
import com.factory.common.bean.Turnover;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.SpeechClient;
import com.factory.rest.feign.TurnoverClient;

import java.util.Map;

/**
 *
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 16:51
 */
public class TurnoverClientFallBack extends BaseFeignClientFallBack<Turnover> implements TurnoverClient {
    @Override
    public Result statistics(Map<String, Object> params) {
        return Result.fail();
    }
}