package com.factory.rest.feign;

import com.alibaba.fastjson.JSONObject;
import com.factory.common.pojo.Result;
import com.factory.common.pojo.SendData;
import com.factory.rest.feign.fallback.AiClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * ai client 微服务调用feign
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/25 17:01
 */
@FeignClient(value = "aiclient", fallback = AiClientFallback.class)
public interface AiClients {

    /**
     * 获取在线的设备列表
     * @param jsonObject
     * @return
     */
    @RequestMapping("/aiclient/index/onLines")
    Result onLines(@RequestBody JSONObject jsonObject);

    /**
     * 检查设备是否在线
     * @param id
     * @return
     */
    @RequestMapping("/aiclient/index/isOnLine/{id}")
    Result isOnLine(@PathVariable String id);

    /**
     * 发送到客户端数据
     * @return
     */
    @RequestMapping(value = "/aiclient/index/sendClient", method = RequestMethod.POST)
    Result sendClient(@RequestBody SendData sendData);

    /**
     * 发送webrtc信令数据
     * @param jsonObject
     * @return
     */
    @PostMapping("/aiclient/index/webRtc")
    Result webRtc(@RequestBody JSONObject jsonObject);
}
