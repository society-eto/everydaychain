package com.factory.rest.feign;

import com.factory.common.bean.Speech;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.fallback.SpeechClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 16:49
 */
@FeignClient(value = "datacenter", fallback = SpeechClientFallBack.class, path = "/datacenter/speech")
public interface SpeechClient extends BaseFeignClient<Speech> {

    /**
     * 数据统计接口
     *
     * @param params
     * @return
     */
    @PostMapping("/statistics")
    Result statistics(@RequestBody Map<String, Object> params);

    @PostMapping("/selectList")
    Result selectList(@RequestBody Map<String, Object> params);
}