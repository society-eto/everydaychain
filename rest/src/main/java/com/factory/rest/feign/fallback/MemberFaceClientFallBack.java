package com.factory.rest.feign.fallback;

import com.factory.common.bean.MemberFace;
import com.factory.rest.feign.MemberFaceClient;

public class MemberFaceClientFallBack extends BaseFeignClientFallBack<MemberFace> implements MemberFaceClient {
}
