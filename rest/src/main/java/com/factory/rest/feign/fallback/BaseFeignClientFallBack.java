package com.factory.rest.feign.fallback;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.factory.common.bean.BaseBean;
import com.factory.common.pojo.QueryListParam;
import com.factory.common.pojo.Result;
import com.factory.common.pojo.UpdateWrapperBody;
import com.factory.rest.feign.BaseFeignClient;

public class BaseFeignClientFallBack<T extends BaseBean>  implements BaseFeignClient<T> {

    @Override
    public Result insert(T t) {
        return Result.fail("service error");
    }

    @Override
    public Result query(String id,String deviceId) {
        return Result.fail("service error");
    }

    @Override
    public Result delete(String id, String deviceId) {
        return Result.fail("service error");
    }

    @Override
    public Result update(T t, String id, String deviceId) {
        return Result.fail("service error");
    }

    @Override
    public Result updateWrapper(UpdateWrapperBody updateWrapperBody) {
        return Result.fail("service error");
    }

    @Override
    public Result queryList(QueryListParam param) {
        return Result.fail("service error");
    }

}
