package com.factory.rest.feign.fallback;

import com.factory.common.bean.CustomersDoorway;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.CustomersDoorwayClient;

import java.util.Map;

/**
 *
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 16:51
 */
public class CustomersDoorwayClientFallBack extends BaseFeignClientFallBack<CustomersDoorway> implements CustomersDoorwayClient {
    @Override
    public Result statistics(Map<String, Object> params) {
        return Result.fail();
    }
}
