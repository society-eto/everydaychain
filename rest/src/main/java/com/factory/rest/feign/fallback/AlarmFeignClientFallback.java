package com.factory.rest.feign.fallback;

import com.factory.common.bean.Alarm;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.AlarmClient;

import java.util.Map;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/25 11:20
 */
public class AlarmFeignClientFallback extends BaseFeignClientFallBack<Alarm> implements AlarmClient {
    @Override
    public Result statistics(Map<String, Object> params) {
        return Result.fail("请求失败!");
    }

    @Override
    public Result statisticsAlarm(Map<String, Object> params) {
        return Result.fail();
    }

    @Override
    public Result statisticsDay(Map<String, Object> params) {
        return Result.fail();
    }

    @Override
    public Result statisticsWeek(Map<String, Object> params) {
        return Result.fail();
    }

    @Override
    public Result statisticsMonth(Map<String, Object> params) {
        return Result.fail();
    }

    @Override
    public Result statisticsQuarter(Map<String, Object> params) {
        return Result.fail();
    }
}
