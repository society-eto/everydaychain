package com.factory.rest.feign;

import com.factory.common.bean.Member;
import com.factory.rest.feign.fallback.MemberClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "datacenter",fallback = MemberClientFallBack.class,path = "/datacenter/member")
public interface MemberClient extends BaseFeignClient<Member>{

}
