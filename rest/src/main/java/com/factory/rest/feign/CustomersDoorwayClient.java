package com.factory.rest.feign;

import com.factory.common.bean.CustomersDoorway;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.fallback.CustomersDoorwayClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 16:50
 */
@FeignClient(value = "datacenter", fallback = CustomersDoorwayClientFallBack.class, path = "/datacenter/customersDoorway")
public interface CustomersDoorwayClient extends BaseFeignClient<CustomersDoorway> {

    /**
     * 数据统计接口
     *
     * @param params
     * @return
     */
    @PostMapping("/statistics")
    Result statistics(@RequestBody Map<String, Object> params);
}