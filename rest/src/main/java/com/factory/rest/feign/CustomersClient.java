package com.factory.rest.feign;

import com.factory.common.bean.Customers;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.fallback.CustomersClientFallBack;
import com.factory.rest.feign.fallback.MemberClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * 会员信息远程调用
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/26 10:17
 */
@FeignClient(value = "datacenter", fallback = CustomersClientFallBack.class, path = "/datacenter/customers")
public interface CustomersClient extends BaseFeignClient<Customers> {

    /**
     * 客流数据日统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsDay")
    Result statisticsDay(@RequestBody Map<String, Object> params);

    /**
     * 客流数据周统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsWeek")
    Result statisticsWeek(@RequestBody Map<String, Object> params);

    /**
     * 客流数据月统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsMonth")
    Result statisticsMonth(@RequestBody Map<String, Object> params);

    /**
     * 客流数据季度统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsQuarter")
    Result statisticsQuarter(@RequestBody Map<String, Object> params);

    /**
     * 性别数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsGender")
    Result statisticsGender(@RequestBody Map<String, Object> params);

    /**
     * 年龄数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsAge")
    Result statisticsAge(@RequestBody Map<String, Object> params);

    /**
     * 年龄数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsAgeCustomers")
    Result statisticsAgeCustomers(@RequestBody Map<String, Object> params);

    /**
     * 店铺数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsLocation")
    Result statisticsLocation(@RequestBody Map<String, Object> params);

    /**
     * 店铺数据统计
     *
     * @param params
     * @return
     */
    @PostMapping("/statisticsCustomers")
    Result statisticsCustomers(@RequestBody Map<String, Object> params);
}
