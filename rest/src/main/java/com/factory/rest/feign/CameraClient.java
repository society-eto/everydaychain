package com.factory.rest.feign;

import com.factory.common.bean.Camera;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.fallback.CameraClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/17 15:03
 */
@FeignClient(value = "datacenter",fallback = CameraClientFallBack.class,path = "/datacenter/camera")
public interface CameraClient extends BaseFeignClient<Camera>{

    /**
     * 摄像头数据统计
     * @param params
     * @return
     */
    @PostMapping("/statistics")
    public Result statistics(@RequestBody Map<String,Object> params);
}
