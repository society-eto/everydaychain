package com.factory.rest.feign;

import com.factory.common.bean.MemberFace;
import com.factory.rest.feign.fallback.MemberFaceClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "datacenter",fallback = MemberFaceClientFallBack.class,path = "/datacenter/memberFace")
public interface MemberFaceClient extends BaseFeignClient<MemberFace>{
}
