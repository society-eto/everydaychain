package com.factory.rest.feign.fallback;

import com.factory.common.bean.Camera;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.CameraClient;

import java.util.Map;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/17 15:05
 */
public class CameraClientFallBack extends BaseFeignClientFallBack<Camera> implements CameraClient {
    @Override
    public Result statistics(Map<String, Object> params) {
        return Result.fail("请求失败!");
    }
}
