package com.factory.rest.feign.fallback;

import com.factory.common.bean.FaceCaptured;
import com.factory.rest.feign.FaceCapturedClient;

public class FaceCapturedClientFallBack extends BaseFeignClientFallBack<FaceCaptured> implements FaceCapturedClient {}