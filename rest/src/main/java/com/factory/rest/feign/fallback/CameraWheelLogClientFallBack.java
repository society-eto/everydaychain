package com.factory.rest.feign.fallback;

import com.factory.common.bean.CameraWheelLog;
import com.factory.common.bean.Speech;
import com.factory.rest.feign.CameraWheelLogClient;
import com.factory.rest.feign.SpeechClient;

/**
 *
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 16:51
 */
public class CameraWheelLogClientFallBack extends BaseFeignClientFallBack<CameraWheelLog> implements CameraWheelLogClient {
}