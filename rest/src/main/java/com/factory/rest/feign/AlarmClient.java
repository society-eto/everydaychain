package com.factory.rest.feign;

import com.factory.common.bean.Alarm;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.fallback.AlarmFeignClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/25 11:18
 */
@FeignClient(value = "datacenter",fallback = AlarmFeignClientFallback.class,path = "/datacenter/alarm")
public interface AlarmClient extends BaseFeignClient<Alarm> {

    /**
     * 告警数据统计
     * @param params
     * @return
     */
    @PostMapping("/statistics")
    Result statistics(@RequestBody Map<String,Object> params);


    /**
     * 告警信息统计
     * @param params
     * @return
     */
    @PostMapping("/statisticsAlarm")
    Result statisticsAlarm(@RequestBody Map<String, Object> params);

    /**
     * 日统计告警信息统计
     * @param params
     * @return
     */
    @PostMapping("/statisticsDay")
    Result statisticsDay(@RequestBody Map<String, Object> params);

    /**
     * 周统计告警信息统计
     * @param params
     * @return
     */
    @PostMapping("/statisticsWeek")
    Result statisticsWeek(@RequestBody Map<String, Object> params);

    /**
     * 周统计告警信息统计
     * @param params
     * @return
     */
    @PostMapping("/statisticsMonth")
    Result statisticsMonth(@RequestBody Map<String, Object> params);

    /**
     * 年统计告警信息统计
     * @param params
     * @return
     */
    @PostMapping("/statisticsQuarter")
    Result statisticsQuarter(@RequestBody Map<String, Object> params);
}
