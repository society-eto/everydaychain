package com.factory.rest.feign;

import com.factory.common.bean.CameraWheelLog;
import com.factory.rest.feign.fallback.CameraWheelLogClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 16:49
 */
@FeignClient(value = "datacenter", fallback = CameraWheelLogClientFallBack.class, path = "/datacenter/cameraWheelLog")
public interface CameraWheelLogClient extends BaseFeignClient<CameraWheelLog> {
}