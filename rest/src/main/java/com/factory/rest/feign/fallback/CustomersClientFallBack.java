package com.factory.rest.feign.fallback;

import com.factory.common.bean.Customers;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.CustomersClient;

import java.util.Map;

public class CustomersClientFallBack extends BaseFeignClientFallBack<Customers> implements CustomersClient {

    @Override
    public Result statisticsDay(Map<String, Object> params) {
        return Result.fail("失败!");
    }

    @Override
    public Result statisticsWeek(Map<String, Object> params) {
        return Result.fail("失败!");
    }

    @Override
    public Result statisticsMonth(Map<String, Object> params) {
        return Result.fail("失败!");
    }

    @Override
    public Result statisticsQuarter(Map<String, Object> params) {
        return Result.fail("失败!");
    }

    @Override
    public Result statisticsGender(Map<String, Object> params) {
        return Result.fail("失败!");
    }

    @Override
    public Result statisticsAge(Map<String, Object> params) {
        return Result.fail("失败!");
    }

    @Override
    public Result statisticsAgeCustomers(Map<String, Object> params) {
        return Result.fail();
    }

    @Override
    public Result statisticsLocation(Map<String, Object> params) {
        return Result.fail("失败!");
    }

    @Override
    public Result statisticsCustomers(Map<String, Object> params) {
        return Result.fail();
    }
}
