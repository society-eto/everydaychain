package com.factory.rest.feign.fallback;

import com.alibaba.fastjson.JSONObject;
import com.factory.common.pojo.Result;
import com.factory.common.pojo.SendData;
import com.factory.rest.feign.AiClients;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/25 17:04
 */
public class AiClientFallback implements AiClients {

    @Override
    public Result onLines(JSONObject jsonObject) {
        return Result.fail("请求失败!");
    }

    @Override
    public Result isOnLine(String id) {
        return Result.fail("请求失败!");
    }

    @Override
    public Result sendClient(SendData sendData) {
        return Result.fail("请求失败!");
    }

    @Override
    public Result webRtc(JSONObject jsonObject) {
        return Result.fail("请求失败!");
    }

}
