package com.factory.rest.feign.fallback;

import com.factory.common.bean.CameraWheelLog;
import com.factory.common.bean.Speech;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.SpeechClient;

import java.util.Map;

/**
 *
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 16:51
 */
public class SpeechClientFallBack extends BaseFeignClientFallBack<Speech> implements SpeechClient {
    @Override
    public Result statistics(Map<String, Object> params) {
        return Result.fail();
    }

    @Override
    public Result selectList(Map<String, Object> params) {
        return Result.fail();
    }

}