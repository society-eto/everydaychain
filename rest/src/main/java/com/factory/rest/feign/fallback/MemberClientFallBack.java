package com.factory.rest.feign.fallback;

import com.factory.common.bean.Member;
import com.factory.rest.feign.MemberClient;

public class MemberClientFallBack extends BaseFeignClientFallBack<Member> implements MemberClient {
}
