package com.factory.rest.feign;

import com.factory.common.bean.FaceCaptured;
import com.factory.rest.feign.fallback.FaceCapturedClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 会员信息远程调用
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/26 10:17
 */
@FeignClient(value = "datacenter",fallback = FaceCapturedClientFallBack.class,path = "/datacenter/faceCaptured")
public interface FaceCapturedClient extends BaseFeignClient<FaceCaptured>{}