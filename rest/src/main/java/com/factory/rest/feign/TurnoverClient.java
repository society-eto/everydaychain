package com.factory.rest.feign;

import com.factory.common.bean.Turnover;
import com.factory.common.pojo.Result;
import com.factory.rest.feign.fallback.TurnoverClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/6 14:12
 */
@FeignClient(value = "datacenter", fallback = TurnoverClientFallBack.class, path = "/datacenter/turnover")
public interface TurnoverClient extends BaseFeignClient<Turnover> {

    /**
     * 营业数据统计接口
     *
     * @param params
     * @return
     */
    @PostMapping("/statistics")
    Result statistics(@RequestBody Map<String, Object> params);
}
