package com.factory.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@EnableScheduling
@EnableTransactionManagement
@EnableAsync
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 43200)
@EnableDiscoveryClient
@EnableFeignClients
public class FactoryRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(FactoryRestApplication.class, args);
	}
}
