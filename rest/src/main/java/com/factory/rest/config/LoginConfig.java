package com.factory.rest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 登录拦截配置
 */
@Configuration
public class LoginConfig implements WebMvcConfigurer {

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/rest/login/**", "/rest/user/**", "/rest/loginOut/**")
                .excludePathPatterns("/rest/client/**")
                .excludePathPatterns("/rest/getImage/**")
                .excludePathPatterns("/rest/index/**")
                .excludePathPatterns("/rest/alarm/send")
                .excludePathPatterns("/rest/webSocket/**","/rest/webRTC/**","/rest/streamer/**")
                .excludePathPatterns("/rest/wx/**");
    }
}