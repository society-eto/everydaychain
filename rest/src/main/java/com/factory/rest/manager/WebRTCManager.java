package com.factory.rest.manager;

import lombok.extern.slf4j.Slf4j;

import javax.websocket.Session;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * websocket管理
 */
@Slf4j
public class WebRTCManager {

    /**
     * 客户端连接session集合 <deviceId,Session></>
     */
    private static Map<String, Session> sessionMap = new ConcurrentHashMap<String, Session>();

    /**
     * 存放新加入的session 便于以后对session管理
     *
     * @param userId  用户id
     * @param session 用户session
     */
    public synchronized static void setSession(String userId, Session session) {
        sessionMap.put(userId, session);
    }

    /**
     * 根据session ID移除session
     *
     * @param session
     */
    public static void closeSession(Session session) {
        if (null == session) {
            return;
        }
        sessionMap.forEach((k, v) -> {
            if (session.getId().equals(v.getId())) {
                sessionMap.remove(k);
                if(session.isOpen()){
                    try {
                        session.close();
                    } catch (IOException e) {
                        log.error("关闭session时发生异常,{}",e);
                    }
                }
            }
        });
    }

    /**
     * 根据session ID移除session
     *
     * @param deviceId
     */
    public static void closeSession(String deviceId) {
        Session session = sessionMap.get(deviceId);
        if (null == session) {
            return;
        }

        sessionMap.remove(deviceId);
        if(session.isOpen()) {
            try {
                session.close();
            } catch (IOException e) {
                log.error("关闭session时发生异常,{}", e);
            }
        }
    }

    /**
     * 给用户发送消息
     *
     * @param deviceId  用户id
     * @param message 消息
     */
    public static boolean sendMessage(String deviceId, String message) {
        log.info("给用户发送消息,{}", deviceId);
        Session session = sessionMap.get(deviceId);
        if (session != null) {
            synchronized (session) {
                try {
                    session.getBasicRemote().sendText(message);
                    return true;
                } catch (IOException e) {
                    log.error("发送websocket消息是异常,{}", e);
                }
            }
        }
        return false;
    }

    public static boolean isOnLine(String id) {
        return sessionMap.containsKey(id);
    }
}