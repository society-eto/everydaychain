package com.factory.aiclient.service;

import com.factory.common.bean.Devices;
import com.factory.common.core.BaseService;

public interface DeviceService extends BaseService<Devices> {
}
