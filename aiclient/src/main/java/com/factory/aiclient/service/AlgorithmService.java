package com.factory.aiclient.service;

import com.factory.common.bean.Algorithm;
import com.factory.common.core.BaseService;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2021/1/11 10:28
 */
public interface AlgorithmService extends BaseService<Algorithm> {
}
