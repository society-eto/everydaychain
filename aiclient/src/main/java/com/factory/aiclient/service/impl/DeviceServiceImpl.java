package com.factory.aiclient.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.aiclient.service.DeviceService;
import com.factory.common.bean.Devices;
import com.factory.common.core.BaseServiceImpl;
import com.factory.common.mapper.DevicesMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DeviceServiceImpl extends BaseServiceImpl<Devices> implements DeviceService {

    @Autowired
    private DevicesMapper devicesMapper;


    @Override
    protected BaseMapper<Devices> getBaseMapper() {
        return this.devicesMapper;
    }

}
