package com.factory.aiclient.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.aiclient.service.AlgorithmService;
import com.factory.common.bean.Algorithm;
import com.factory.common.core.BaseServiceImpl;
import com.factory.common.mapper.AlgorithmMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2021/1/11 10:29
 */
@Service
public class AlgorithmServiceImpl extends BaseServiceImpl<Algorithm> implements AlgorithmService {

    @Autowired
    private AlgorithmMapper algorithmMapper;

    @Override
    protected BaseMapper<Algorithm> getBaseMapper() {
        return this.algorithmMapper;
    }

}
