package com.factory.aiclient.service;

import com.factory.common.bean.UserDevices;
import com.factory.common.core.BaseService;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/27 11:23
 */
public interface UserDeviceService extends BaseService<UserDevices> {

        String CurrentDevice = "CurrentDevice";

        /**
         * 获取当前设备
         * @return
         */
        UserDevices getCurrent(String id);

        /**
         * 跟新设备和缓存
         */
        Integer update(UserDevices userDevices);
}