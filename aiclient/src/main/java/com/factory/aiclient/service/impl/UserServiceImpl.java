package com.factory.aiclient.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.aiclient.service.UserService;
import com.factory.common.bean.User;
import com.factory.common.core.BaseServiceImpl;
import com.factory.common.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    protected BaseMapper<User> getBaseMapper() {
        return this.userMapper;
    }

    @Override
    public User getByAccount(String account) {
        return userMapper.selectOne(new QueryWrapper<User>().eq("account",account));
    }
}
