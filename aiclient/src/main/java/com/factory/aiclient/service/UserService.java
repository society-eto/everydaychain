package com.factory.aiclient.service;

import com.factory.common.bean.User;
import com.factory.common.core.BaseService;

public interface UserService extends BaseService<User> {

    /**
     * 根据账号查询用户
     * @param account
     * @return
     */
    User getByAccount(String account);
}
