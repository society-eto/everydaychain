package com.factory.aiclient.service;

/**
 * 异步处理service
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/18 9:06
 */
public interface AsyncService {

    /**
     * 将base64图片写入到本地磁盘
     * @param base64Str
     * @param path
     * @param fileName
     */
    void writeBase64Image(String base64Str, String path, String fileName);
}
