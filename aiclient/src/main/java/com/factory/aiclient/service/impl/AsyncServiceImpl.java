package com.factory.aiclient.service.impl;

import com.factory.aiclient.service.AsyncService;
import com.factory.common.utils.DateUtil;
import com.factory.common.utils.ImgUtil;
import com.factory.common.utils.StringUtil;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Date;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/18 9:07
 */
@Service
@Async
public class AsyncServiceImpl implements AsyncService {

    @Override
    public void writeBase64Image(String base64Str, String path, String fileName) {


        if (StringUtil.notEmpty(base64Str) && StringUtil.notEmpty(path) && StringUtil.notEmpty(fileName)) {
            ImgUtil.generateBase64Image(base64Str, path , fileName);
        }
    }

}
