package com.factory.aiclient.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.factory.aiclient.service.UserDeviceService;
import com.factory.common.bean.UserDevices;
import com.factory.common.constant.DeviceConstant;
import com.factory.common.core.BaseServiceImpl;
import com.factory.common.mapper.UserDevicesMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户设备管理service类
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/27 11:24
 */
@Service
@Slf4j
public class UserDeviceServiceImpl extends BaseServiceImpl<UserDevices> implements UserDeviceService {

    @Autowired
    private UserDevicesMapper userDevicesMapper;

    @Override
    protected BaseMapper<UserDevices> getBaseMapper() {
        return this.userDevicesMapper;
    }

    @Override
    @Cacheable(value = "userDevices", key = "#id", unless = "#result == null")
    public UserDevices getCurrent(String id) {
        log.info("当前设备id为:{}", id);
        return userDevicesMapper.selectById(id);
    }

    @Override
    @CacheEvict(value = "userDevices",key = "#userDevices.id")
    public Integer update(UserDevices userDevices) {
        return userDevicesMapper.updateById(userDevices);
    }
}
