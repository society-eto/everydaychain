package com.factory.aiclient.service;

import com.factory.common.bean.Location;
import com.factory.common.core.BaseService;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/10 9:31
 */
public interface LocationService extends BaseService<Location> {
}
