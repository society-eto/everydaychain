package com.factory.aiclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAsync
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.factory.aiclient.feign")
@EnableCaching
public class AIClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(AIClientApplication.class,args);
    }
}
