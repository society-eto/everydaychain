package com.factory.aiclient.webSocket;

import com.factory.common.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;

import javax.websocket.Session;
import java.io.IOException;

/**
 * 写数据的
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/26 13:48
 */
@Slf4j
public class PlaybackReader implements Runnable {

    private Boolean isRun = true;

    private Session session;

    private String cameraId;

    public PlaybackReader(String cameraId, Session session) {
        this.session = session;
        this.cameraId = cameraId;
    }

    @Override
    public void run() {
        while (isRun) {
            try {
                String read = PlaybackPool.read(cameraId);
                if(StringUtil.notEmpty(read)){
                    session.getBasicRemote().sendText(read);
                    Thread.sleep(80l);
                }
                //session.getBasicRemote().sendBinary(ByteBuffer.wrap(VideoPool.readBytes(cameraId)));
            } catch (IOException e) {
                log.error("向socket写数据时异常,{}", e);
            } catch (InterruptedException e) {
                log.error("向socket写数据时异常,{}", e);
            }
        }
    }

    public void stop() {
        this.isRun = false;
    }

    public Boolean isRun() {
        return isRun;
    }

    public Session getSession() {
        return session;
    }

    public String getCameraId() {
        return cameraId;
    }
}
