package com.factory.aiclient.webSocket;

import lombok.extern.slf4j.Slf4j;

import javax.websocket.Session;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 视频数据缓冲池子
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/26 11:23
 */
@Slf4j
public class VideoPool {

    private static Map<String, String> base64Images = new ConcurrentHashMap<String,String>();

    private static Map<String, byte[]> bytesMap = new ConcurrentHashMap<>();

    public static void write(Session session, String base64Image){
        base64Images.put(VideoManager.getCameraId(session.getId()),base64Image);
    }

    public static void writeByte(Session session, byte[] bytes){
        bytesMap.put(VideoManager.getCameraId(session.getId()),bytes);
    }

    public static String read(String cameraId){
        return base64Images.get(cameraId);
    }

    public static byte[] readBytes(String cameraId){
        return bytesMap.get(cameraId);
    }
}
