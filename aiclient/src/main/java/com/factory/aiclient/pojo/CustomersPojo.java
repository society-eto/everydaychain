package com.factory.aiclient.pojo;

import com.factory.common.bean.Customers;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/28 17:03
 */
public class CustomersPojo extends Customers {

    private Integer method;

    public Integer getMethod() {
        return method;
    }

    public void setMethod(Integer method) {
        this.method = method;
    }
}
