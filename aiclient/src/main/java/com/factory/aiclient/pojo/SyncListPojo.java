package com.factory.aiclient.pojo;

import java.util.List;

/**
 * 数据同步包装类
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/3 15:56
 */
public class SyncListPojo<T> {

    private List<T> data;

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
