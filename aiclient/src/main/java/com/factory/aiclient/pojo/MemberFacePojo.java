package com.factory.aiclient.pojo;

import com.factory.common.bean.MemberFace;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;

import java.util.Optional;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/28 17:05
 */
public class MemberFacePojo extends MemberFace {

    private Integer method;

    private String image;

    public Integer getMethod() {
        return method;
    }

    public void setMethod(Integer method) {
        this.method = method;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Optional<Result> validate(){

        if(StringUtil.isEmpty(getId())){
            return Optional.of(Result.fail("数据同步id不能为空"));
        }else if(StringUtil.isEmpty(getMethod())){
            return Optional.of(Result.fail("操作方法不能为空"));
        }

        return Optional.empty();
    }
}
