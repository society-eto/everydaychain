package com.factory.aiclient.pojo;

import com.factory.common.bean.Camera;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;

import java.util.Optional;

/**
 * 摄像头包装类
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/17 15:08
 */
public class CameraPojo extends Camera {

    private Integer method;

    public Integer getMethod() {
        return method;
    }

    public void setMethod(Integer method) {
        this.method = method;
    }

    @Override
    public Optional<Result> validate() {
        if(StringUtil.isEmpty(getId()))
            return Optional.of(Result.fail("id不能为空"));
        else if(StringUtil.isEmpty(getName()))
            return Optional.of(Result.fail("name不能为空"));
        else if(StringUtil.isEmpty(getAccount()))
            return Optional.of(Result.fail("account不能为空"));
        else if(StringUtil.isEmpty(getPassword()))
            return Optional.of(Result.fail("password不能为空"));
        else if(StringUtil.isEmpty(getPort()))
            return Optional.of(Result.fail("port不能为空"));
        else if(StringUtil.isEmpty(getIp()))
            return Optional.of(Result.fail("ip不能为空"));
        else if(StringUtil.isEmpty(getMethod()))
            return Optional.of(Result.fail("method不能为空"));
        else if(StringUtil.isEmpty(getType()))
            return Optional.of(Result.fail("type不能为空"));

        return Optional.empty();
    }
}