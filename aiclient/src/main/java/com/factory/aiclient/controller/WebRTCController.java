package com.factory.aiclient.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.factory.aiclient.feign.RestClient;
import com.factory.aiclient.manager.SocketManager;
import com.factory.aiclient.manager.WebRTCManager;
import com.factory.aiclient.util.SpringCtxUtils;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * webRTC控制器
 *
 * @author JW
 * @version 1.0
 * @date 2021/2/1 10:18
 */
@ServerEndpoint("/aiclient/webRTC/{deviceId}")
@Controller
@Slf4j
@CrossOrigin
public class WebRTCController {

    private RestClient restClient = SpringCtxUtils.getBean(RestClient.class);

    @OnOpen
    public void onOpen(Session session, @PathParam(value = "deviceId") String deviceId) {
        //获取连接的用户
        log.info("加入session:" + deviceId);
        if (WebRTCManager.isOnLine(deviceId)) {
            log.warn("该设备已有连接,{}", deviceId);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userId",deviceId);
            JSONObject message = new JSONObject();
            message.put("type","close");
            jsonObject.put("message",message);
            WebRTCManager.sendMessage(deviceId,jsonObject.toJSONString());
            log.info("关闭原有的连接...............................");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error("发生异常,{}",e);
            }
            WebRTCManager.closeSession(deviceId);
        }
        WebRTCManager.setSession(deviceId, session);
    }

    @OnClose
    public void onClose(Session session) {
        log.info("移除不用的session:" + session.getId());
        WebRTCManager.closeSession(session);
    }


    //收到客户端信息
    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
        log.debug("message,{}", session.getId());
        JSONObject jsonObject;

        try {
            jsonObject = JSON.parseObject(message);
        }catch (JSONException jsonException){
            log.error("格式化json错误!");
            return;
        }

        if (!jsonObject.containsKey("heart")){
            restClient.webRtc(jsonObject);
//            WebRTCManager.sendMessage(jsonObject.getString("toUserId"), jsonObject.toJSONString());
        }
    }

    //错误时调用
    @OnError
    public void onError(Session session, Throwable throwable) {
        log.error("webSocket 错误,{}", throwable.getMessage());
        WebRTCManager.closeSession(session);
    }

}