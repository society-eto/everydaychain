package com.factory.aiclient.controller;

import com.factory.aiclient.feign.*;
import com.factory.aiclient.service.UserDeviceService;
import com.factory.common.bean.UserDevices;
import com.factory.common.core.SqlCondition;
import com.factory.common.pojo.QueryListParam;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 数据同步控制器
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/26 10:19
 */
@RestController
@RequestMapping("/aiclient/pull")
@Slf4j
public class PullController {

    @Autowired
    private MemberClient memberClient;

    @Autowired
    private MemberFaceClient memberFaceClient;

    @Autowired
    private UserDeviceService userDeviceService;

    /**
     * 同步会员数据
     *
     * @return
     */
    @PostMapping("/member")
    public Result member(HttpServletRequest request, @RequestBody Map<String,Object> param) {
        log.info("同步会员数据,{}", param);

        UserDevices current = (UserDevices) request.getAttribute(UserDeviceService.CurrentDevice);
        if(null == current){
            return Result.fail("设备数据为空");
        }

        QueryListParam queryListParam = new QueryListParam();
        queryListParam.setPageNum(1);
        queryListParam.setPageSize(1000);

        List<SqlCondition> conditions = new ArrayList<>();
        conditions.add(new SqlCondition("userId",current.getUserId(),SqlCondition.Conditions.EQ));
        conditions.add(new SqlCondition("deviceId",current.getId(),SqlCondition.Conditions.NE));
        queryListParam.setConditions(conditions);

        if(StringUtil.notEmpty(param.get("updateDate"))){
            conditions.add(new SqlCondition("updateDate",param.get("updateDate").toString(),SqlCondition.Conditions.GE));
        }
        return memberClient.queryList(queryListParam);
    }

    /**
     * 会员人脸数据同步
     *
     * @return
     */
    @PostMapping("/memberFace")
    public Result memberFace(HttpServletRequest request, @RequestBody Map<String,Object> param) {
        log.info("会员人脸数据拉取,{}", param);

        UserDevices current = (UserDevices) request.getAttribute(UserDeviceService.CurrentDevice);
        if(null == current){
            return Result.fail("设备数据为空");
        }

        QueryListParam queryListParam = new QueryListParam();
        queryListParam.setPageNum(1);
        queryListParam.setPageSize(1000);

        List<SqlCondition> conditions = new ArrayList<>();
        conditions.add(new SqlCondition("userId",current.getUserId(),SqlCondition.Conditions.EQ));
        conditions.add(new SqlCondition("deviceId",current.getId(),SqlCondition.Conditions.NE));
        queryListParam.setConditions(conditions);

        if(StringUtil.notEmpty(param.get("updateDate"))){
            conditions.add(new SqlCondition("updateDate",param.get("updateDate").toString(),SqlCondition.Conditions.GE));
        }
        return memberFaceClient.queryList(queryListParam);
    }
}
