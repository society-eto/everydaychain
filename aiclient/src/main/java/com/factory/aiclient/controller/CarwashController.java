package com.factory.aiclient.controller;

import com.alibaba.fastjson.JSONObject;
import com.factory.aiclient.service.DeviceService;
import com.factory.aiclient.service.LocationService;
import com.factory.aiclient.service.UserDeviceService;
import com.factory.aiclient.service.UserService;
import com.factory.common.bean.Location;
import com.factory.common.bean.User;
import com.factory.common.bean.UserDevices;
import com.factory.common.constant.DeviceConstant;
import com.factory.common.pojo.Result;
import com.factory.common.utils.HttpUtil;
import com.factory.common.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 洗车机调用接口控制类
 *
 * @date 2021-01-27
 * @author JW
 *
 */
@RestController
@RequestMapping("/aiclient/carwash")
@Slf4j
public class CarwashController {

    @Autowired
    private UserDeviceService userDeviceService;

    /**
     * 	请求参数
     * {
     *     "deviceId":"设备ID "
     * }
     * 	响应参数
     * {
     *     "status": '成功true 失败 false',
     *     "data": null,
     *     "message": "返回信息",
     *     "code": 状态码
     * }
     *
     * @param param
     * @return
     */
    @PostMapping("/start")
    public Result start(@RequestBody Map<String, Object> param) {
        log.info("开启洗车机接口,{}", param);
        if(StringUtil.isEmpty(param.get("deviceId"))){
            return Result.fail("deviceId不能为空");
        }
        UserDevices devices = userDeviceService.selectById(param.get("deviceId").toString());
        if(null == devices){
            return Result.fail("设备不存在");
        }

        String domain = devices.getDomain();
        if(StringUtil.isEmpty(domain)){
            return Result.fail("域名未配置");
        }

        JSONObject json = new JSONObject();
        JSONObject result = HttpUtil.post2(domain + "/device/start_detect", json.toJSONString());

        return new Result(result.getBoolean("status"), result.getString("data") ,result.getString("message"),200);
    }

    /**
     * 	请求参数
     * {
     *     "deviceId":"设备ID "
     * }
     * 	响应参数
     * {
     *     "status": "成功true失败false",
     *     "data":
     *      {
     * 		    "deviceId ":  "设备ID",
     *          "timePoint":  "请求时间",
     *          "result":  "返回检测结果ok/left/right/error", //ok正常left左侧越界right右侧越界error发生错误all左右都越界none无法识别
     *          "message": "请求检测结果（字符串）"
     *      },
     *     "message": "返回信息",
     *     "code": 状态码
     * }
     *
     * @param param
     * @return
     */
    @PostMapping("/status")
    public Result status(@RequestBody Map<String, Object> param) {
        log.info("获取洗车机状态接口,{}", param);
        Map<String,Object> data = new HashMap<>();

        if(StringUtil.isEmpty(param.get("deviceId"))){
            data.put("result", "error");
            data.put("message","deviceId不能为空");
            return Result.fail(data,"deviceId不能为空");
        }

        UserDevices devices = userDeviceService.selectById(param.get("deviceId").toString());
        if(null == devices){
            data.put("result", "error");
            data.put("message","设备不存在");
            return Result.fail(data,"设备不存在");
        }

        String domain = devices.getDomain();
        if(StringUtil.isEmpty(domain)){
            data.put("result", "error");
            data.put("message","域名未配置");
            return Result.fail(data,"域名未配置");
        }

        JSONObject json = new JSONObject();
        JSONObject result = HttpUtil.post2(domain + "/device/close_detect", json.toJSONString());

        data.put("deviceId",devices.getId());
        data.put("timePoint",System.currentTimeMillis());

        String resultString = result.getString("data");
        if("all".equals(resultString)){
            resultString = "left";
        }else if("null".equals(resultString)){
            resultString = "error";
        }

        data.put("result", result.getBoolean("status") ? resultString : "error");
        data.put("message",result.getBoolean("status") ? result.getString("message") : "请求错误!");

        return Result.success(data);
    }
}