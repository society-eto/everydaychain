package com.factory.aiclient.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.factory.aiclient.manager.SocketManager;
import com.factory.aiclient.pojo.LocationPojo;
import com.factory.aiclient.service.DeviceService;
import com.factory.aiclient.service.LocationService;
import com.factory.aiclient.service.UserDeviceService;
import com.factory.aiclient.service.UserService;
import com.factory.common.bean.Devices;
import com.factory.common.bean.Location;
import com.factory.common.bean.User;
import com.factory.common.bean.UserDevices;
import com.factory.common.constant.DeviceConstant;
import com.factory.common.pojo.Result;
import com.factory.common.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * 设备控制
 */
@RestController
@RequestMapping("/aiclient/device")
@Slf4j
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private UserDeviceService userDeviceService;

    @Autowired
    private UserService userService;

    @Autowired
    private LocationService locationService;

    /**
     * 设备激活接口
     *
     * @param param
     * @return
     */
    @PostMapping("/active")
    public Result active(HttpServletRequest request, @RequestBody Map<String, Object> param) {
        log.info("设备激活,{}", param);

        UserDevices current = (UserDevices) request.getAttribute(UserDeviceService.CurrentDevice);
        if(null == current)
            return Result.fail("设备不存在");
        else if (StringUtil.isEmpty(param.get("username")))
            return Result.fail("请输入用户名");
        else if (StringUtil.isEmpty(param.get("password")))
            return Result.fail("请输入密码");
        else if(DeviceConstant.Status.NORMAL.equals(current.getStatus()))
            return Result.fail("已经激活过了!");

        if(StringUtil.isEmpty(param.get("locationId")))
            return Result.fail("位置信息不能为空!");
        Location location = locationService.selectById(param.get("locationId").toString());
        if(null == location)
            return Result.fail("位置信息不存在!");

        User user = userService.getByAccount(param.get("username").toString());
        if (null == user)
            return Result.fail("用户不存在");
        else if (!user.getPassword().equalsIgnoreCase(DigestUtils.md5DigestAsHex(param.get("password").toString().getBytes())))
            return Result.fail("输入密码不正确");

        current.setUserId(user.getId());
        current.setUserName(user.getName());
        current.setStatus(DeviceConstant.Status.NORMAL);
        current.setLocationId(location.getId());
        current.setLocationName(location.getName());

        userDeviceService.update(current);

        return Result.success(current, "激活成功!");
    }
}