package com.factory.aiclient.feign;

import com.factory.aiclient.feign.fallback.CameraClientFallBack;
import com.factory.common.bean.Camera;
import com.factory.common.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/17 15:03
 */
@FeignClient(value = "datacenter",fallback = CameraClientFallBack.class,path = "/datacenter/camera")
public interface CameraClient extends BaseFeignClient<Camera>{

    /**
     * 更新摄像头的状态
     * @Param cameraIds
     * @Param deviceId
     * @return
     */
    @PostMapping("/updateStatus")
    Result updateStatus(@RequestBody Map<String,Object> params);
}