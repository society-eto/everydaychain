package com.factory.aiclient.feign;

import com.factory.aiclient.feign.fallback.MemberClientFallBack;
import com.factory.common.bean.Customers;
import com.factory.common.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 会员信息远程调用
 *
 * @author JW
 * @version 1.0
 * @date 2020/10/26 10:17
 */
@FeignClient(value = "datacenter",fallback = MemberClientFallBack.class,path = "/datacenter/customers")
public interface CustomersClient extends BaseFeignClient<Customers>{

    /**
     * 批量写入客流数据
     * @param customers
     * @return
     */
    @PostMapping("/inserts")
    Result inserts(@RequestBody List<Customers> customers);
}
