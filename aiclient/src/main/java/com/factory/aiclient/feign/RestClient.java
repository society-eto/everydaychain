package com.factory.aiclient.feign;

import com.alibaba.fastjson.JSONObject;
import com.factory.aiclient.feign.fallback.RestClientFallback;
import com.factory.common.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * 调用rest微服务发送信息
 *
 * @author JW
 * @version 1.0
 * @date 2021/1/11 10:16
 */
@FeignClient(value = "rest",fallback = RestClientFallback.class)
public interface RestClient {

    /**
     * 发送告警数据
     *
     * @return
     */
    @RequestMapping("/rest/index/webRtc")
    Result webRtc(@RequestBody JSONObject jsonObject);

    /**
     * 发送设备状态数据
     * @param jsonObject
     * @return
     */
    @PostMapping("/rest/index/deviceStatus")
    Result deviceStatus(@RequestBody JSONObject jsonObject);

}
