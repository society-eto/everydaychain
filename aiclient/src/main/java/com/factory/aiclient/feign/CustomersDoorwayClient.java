package com.factory.aiclient.feign;

import com.factory.aiclient.feign.fallback.CustomersDoorwayClientFallBack;
import com.factory.common.bean.CustomersDoorway;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 16:50
 */
@FeignClient(value = "datacenter", fallback = CustomersDoorwayClientFallBack.class, path = "/datacenter/customersDoorway")
public interface CustomersDoorwayClient extends BaseFeignClient<CustomersDoorway> {
}
