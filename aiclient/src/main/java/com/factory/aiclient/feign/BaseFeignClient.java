package com.factory.aiclient.feign;

import com.factory.common.bean.BaseBean;
import com.factory.common.pojo.QueryListParam;
import com.factory.common.pojo.Result;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 封装的通用的feign的微服务调用类
 * @param <T>
 */
public interface BaseFeignClient<T extends BaseBean> {

    /**
     * T
     * 通用的添加方法
     *
     * @return 用户列表
     */

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    Result insert(@RequestBody T t);


    /**
     * T
     * 通用的批量添加方法
     *
     * @return 用户列表
     */

    @RequestMapping(value = "/inserts", method = RequestMethod.POST)
    Result inserts(@RequestBody List<T> ts);

    /**
     * 通用的查一条数据方法
     *
     * @return 一条数据
     */
    @RequestMapping(value = "/query/{id}/{deviceId}", method = RequestMethod.GET)
    Result query(@PathVariable(name = "id") String id, @PathVariable(name = "deviceId")String deviceId);

    /**
     * 通用的删除一条数据方法
     *
     * @return 删除一条数据
     */
    @RequestMapping(value = "/del/{id}/{deviceId}", method = RequestMethod.GET)
    Result delete(@PathVariable(name = "id") String id, @PathVariable(name = "deviceId") String deviceId);

    /**
     * 通用的修改一条数据方法
     *
     * @return 修改一条数据
     */

    @RequestMapping(value = "/update/{id}/{deviceId}", method = RequestMethod.POST)
    Result update(@RequestBody T t,@PathVariable(name = "id") String id, @PathVariable(name = "deviceId") String deviceId);

    /**
     * 通用查询数据列表的方法，含有分页的功能
     * 子类继承父类即可使用该方法
     * {
     * "pageSize":10,
     * "pageNum":1,
     * "conditions":[{"name":"account","value":"123456","method":"eq"}]
     * }
     *
     * @return 查询到的数据列表集合
     */
    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    Result queryList(@RequestBody QueryListParam param);
}
