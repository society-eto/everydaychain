package com.factory.aiclient.feign.fallback;

import com.factory.aiclient.feign.AlarmClient;
import com.factory.common.bean.Alarm;

/**
 * 告警fall back
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/8 9:38
 */
public class AlarmClientFallBack extends BaseFeignClientFallBack<Alarm> implements AlarmClient {
}
