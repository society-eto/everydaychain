package com.factory.aiclient.feign.fallback;

import com.factory.aiclient.feign.MemberClient;
import com.factory.common.bean.Member;

public class MemberClientFallBack extends BaseFeignClientFallBack<Member> implements MemberClient {
}
