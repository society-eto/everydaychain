package com.factory.aiclient.feign.fallback;

import com.factory.aiclient.feign.MemberFaceClient;
import com.factory.common.bean.MemberFace;
import com.factory.common.pojo.Result;

import java.util.List;

public class MemberFaceClientFallBack extends BaseFeignClientFallBack<MemberFace> implements MemberFaceClient {
    @Override
    public Result inserts(List<MemberFace> memberFaces) {
        return Result.fail("请求失败!");
    }
}
