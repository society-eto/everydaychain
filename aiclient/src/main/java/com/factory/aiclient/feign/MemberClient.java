package com.factory.aiclient.feign;

import com.factory.aiclient.feign.fallback.MemberClientFallBack;
import com.factory.common.bean.Member;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "datacenter",fallback = MemberClientFallBack.class,path = "/datacenter/member")
public interface MemberClient extends BaseFeignClient<Member>{

}
