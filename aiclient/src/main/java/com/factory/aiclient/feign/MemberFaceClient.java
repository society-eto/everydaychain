package com.factory.aiclient.feign;

import com.factory.aiclient.feign.fallback.MemberFaceClientFallBack;
import com.factory.common.bean.Customers;
import com.factory.common.bean.Member;
import com.factory.common.bean.MemberFace;
import com.factory.common.pojo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(value = "datacenter",fallback = MemberFaceClientFallBack.class,path = "/datacenter/memberFace")
public interface MemberFaceClient extends BaseFeignClient<MemberFace>{

    /**
     * 批量写入客流数据
     * @param memberFaces
     * @return
     */
    @PostMapping("/inserts")
    Result inserts(@RequestBody List<MemberFace> memberFaces);
}
