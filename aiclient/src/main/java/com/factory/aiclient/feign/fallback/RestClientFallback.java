package com.factory.aiclient.feign.fallback;

import com.alibaba.fastjson.JSONObject;
import com.factory.aiclient.feign.RestClient;
import com.factory.common.pojo.Result;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * rest微服务调用失败类
 *
 * @author JW
 * @version 1.0
 * @date 2021/1/11 10:16
 */
public class RestClientFallback implements RestClient {

    @Override
    public Result webRtc(@RequestBody JSONObject jsonObject) {
        return Result.fail();
    }

    @Override
    public Result deviceStatus(JSONObject jsonObject) {
        return Result.fail();
    }

}
