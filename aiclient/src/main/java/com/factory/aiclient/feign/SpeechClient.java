package com.factory.aiclient.feign;

import com.factory.aiclient.feign.fallback.SpeechClientFallBack;
import com.factory.common.bean.CameraWheelLog;
import com.factory.common.bean.Speech;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 16:49
 */
@FeignClient(value = "datacenter",fallback = SpeechClientFallBack.class,path = "/datacenter/speech")
public interface SpeechClient extends BaseFeignClient<Speech> {
}
