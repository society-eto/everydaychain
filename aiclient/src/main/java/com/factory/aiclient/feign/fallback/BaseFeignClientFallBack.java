package com.factory.aiclient.feign.fallback;

import com.factory.aiclient.feign.BaseFeignClient;
import com.factory.common.bean.BaseBean;
import com.factory.common.pojo.QueryListParam;
import com.factory.common.pojo.Result;

import java.util.List;

public class BaseFeignClientFallBack<T extends BaseBean> implements BaseFeignClient<T> {

    @Override
    public Result insert(T t) {
        return Result.fail("service error");
    }

    @Override
    public Result inserts(List<T> ts) {
        return Result.fail("service error");
    }

    @Override
    public Result query(String id,String deviceId) {
        return Result.fail("service error");
    }

    @Override
    public Result delete(String id, String deviceId) {
        return Result.fail("service error");
    }

    @Override
    public Result update(T t, String id, String deviceId) {
        return Result.fail("service error");
    }

    @Override
    public Result queryList(QueryListParam param) {
        return Result.fail("service error");
    }

}
