package com.factory.aiclient.feign.fallback;

import com.factory.aiclient.feign.CustomersClient;
import com.factory.aiclient.feign.MemberClient;
import com.factory.common.bean.Customers;
import com.factory.common.pojo.Result;

import java.util.List;

public class CustomersClientFallBack extends BaseFeignClientFallBack<Customers> implements CustomersClient {

    @Override
    public Result inserts(List<Customers> customers) {
        return Result.fail("请求失败!");
    }

}
