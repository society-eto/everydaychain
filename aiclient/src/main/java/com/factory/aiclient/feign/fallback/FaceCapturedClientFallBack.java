package com.factory.aiclient.feign.fallback;

import com.factory.aiclient.feign.FaceCapturedClient;
import com.factory.common.bean.FaceCaptured;

public class FaceCapturedClientFallBack extends BaseFeignClientFallBack<FaceCaptured> implements FaceCapturedClient {}