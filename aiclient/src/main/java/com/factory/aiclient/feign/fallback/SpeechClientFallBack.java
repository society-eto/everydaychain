package com.factory.aiclient.feign.fallback;

import com.factory.aiclient.feign.SpeechClient;
import com.factory.common.bean.CameraWheelLog;
import com.factory.common.bean.Speech;

/**
 *
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 16:51
 */
public class SpeechClientFallBack extends BaseFeignClientFallBack<Speech> implements SpeechClient {
}
