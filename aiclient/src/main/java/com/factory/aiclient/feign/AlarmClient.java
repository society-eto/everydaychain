package com.factory.aiclient.feign;

import com.factory.aiclient.feign.fallback.AlarmClientFallBack;
import com.factory.common.bean.Alarm;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 告警feign调用
 *
 * @author JW
 * @version 1.0
 * @date 2020/12/8 9:37
 */
@FeignClient(value = "datacenter",fallback = AlarmClientFallBack.class,path = "/datacenter/alarm")
public interface AlarmClient extends BaseFeignClient<Alarm>{
}
