package com.factory.aiclient.feign.fallback;

import com.factory.aiclient.feign.CustomersDoorwayClient;
import com.factory.common.bean.CustomersDoorway;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2021/6/5 16:51
 */
public class CustomersDoorwayClientFallBack extends BaseFeignClientFallBack<CustomersDoorway> implements CustomersDoorwayClient {
}
