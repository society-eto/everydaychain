package com.factory.aiclient.feign.fallback;

import com.factory.aiclient.feign.CameraClient;
import com.factory.common.bean.Camera;
import com.factory.common.pojo.Result;

import java.util.Map;

/**
 * TODO
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/17 15:05
 */
public class CameraClientFallBack extends BaseFeignClientFallBack<Camera> implements CameraClient {
    @Override
    public Result updateStatus(Map<String, Object> params) {
        return Result.fail();
    }
}