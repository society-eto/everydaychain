package com.factory.aiclient.manager;

import com.factory.common.bean.UserDevices;

import javax.websocket.Session;

/**
 * 设备session
 *
 * @author JW
 * @version 1.0
 * @date 2020/11/26 9:41
 */
public class DeviceSession {

    private Session session;

    private UserDevices userDevices;

    public DeviceSession(Session session, UserDevices userDevices) {
        this.session = session;
        this.userDevices = userDevices;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public UserDevices getUserDevices() {
        return userDevices;
    }

    public void setUserDevices(UserDevices userDevices) {
        this.userDevices = userDevices;
    }
}
