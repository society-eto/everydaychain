# everyday chain 

#### 介绍

便利店客流分析云服务端服务器代码
 
- aiclient 边缘设备交互微服务
- rest Web页面、小程序业务微服务
- manager 运营管理平台微服务
- common 通用代码模块
- getaway 路由微服务
- datacenter 数据中心微服务

#### 软件架构

- Spring Alibaba Cloud

#### 项目环境依赖

- Redis数据库
- Mysql数据库
- Nacos服务注册中心